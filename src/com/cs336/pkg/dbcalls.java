package com.cs336.pkg;

import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;


public class dbcalls {
	/*public Connection getConnect() {
		ApplicationDB db = new ApplicationDB();	
		Connection con = db.getConnection();
		return con;
	}*/
	
//PARSES THE XML TAGS THE FOLLOWING METHODS IN THIS FILE CREATE FROM SQL RESULT SETS
	public String getTagContent(String s, String tag) {      
        
        int start = 0, end = 0;
        java.lang.String temp = null;
       
        if(s != null) {
            while(s.indexOf("<"+tag+">", end) != -1) {
                start = s.indexOf("<"+tag+">", end)+("<"+tag+">").length();  
                end = s.indexOf("</"+tag+">", start);
                temp = s.substring(start, end);
            }
       }
       return temp;
    }

	
//CHECKS IF THE SUBMITTED EMAIL EXISTS WHEN CHECKING SUBMITTED LOG IN CREDENTIALS
	public boolean emailExists(String email, boolean manage_custrep) {
		ApplicationDB db = new ApplicationDB();	
		Connection con = db.getConnection();
		boolean ans = false;
		try {
			String query = "";
			if(manage_custrep) {
				query = "select username from manage_custrep where username = '"+email+"'";
			}else {
				query = "select email from customer where email = '"+email+"';";
			}
			System.out.println(query);
			PreparedStatement pscheck = con.prepareStatement(query);
			ResultSet result = pscheck.executeQuery();
			
			if(result.next()) {
				ans = true;
			}
		}catch(Exception e) {
			System.out.println("ERROR in emailExists: "+e);
		}
		
		try {
			db.closeConnection(con);
		}catch(Exception e) {}
		
		return ans;
	}

//TAKES INPUTTED VALUES TO ADD CUSTOMER 
	public String addCustomer(String first, String last, String address, String city, String state, String zip, String phone, String email, String credit_card, String password) {
		Calendar calendar = Calendar.getInstance();
	    java.sql.Date create_date = new java.sql.Date(calendar.getTime().getTime());
	    
	   // calendar.setTime(create_date);
	   // int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
	   // System.out.println(dayOfWeek);
	    
	    System.out.println(create_date);
	    String error = "";
	    ApplicationDB db = new ApplicationDB();	
		Connection con = db.getConnection();
	    try {
	    	
	    	String query = "insert into customer(firstName, lastName, phone, email, address, city, state, zipCode, createDate, password, creditCard) "
				+ "values ('"+first+"','"+last+"','"+phone+"','"+email+"','"+address+"','"+city+"','"+state+"','"+zip+"','"+create_date+"','"+password+"','"+credit_card+"');";
	    	System.out.println(query);
	    	PreparedStatement customer = con.prepareStatement(query);
	    	customer.execute();
	    	
	    }catch(Exception e) {
	    	System.out.println("ERROR in addCustomer: "+e);
	    	error = "Error in adding customer.  Check format of what you are trying to input";
	    }
	    
	    try {
	    	db.closeConnection(con);
		}catch(Exception e) {}
		return error;
	}

//CHECK PASSWORD AGAINST CREDENTIALS STORED IN THE SQL DATABASE
	public boolean passwordCheck(String email, String password, boolean manage_custrep) {
		ApplicationDB db = new ApplicationDB();	
		Connection con = db.getConnection();
		boolean ans = false;
		try {
			String query = "";
			if(manage_custrep) {
				query = "select password from manage_custrep where username = '"+email+"' and password = '"+password+"'";
			}else{
				query = "select password from customer where email = '"+email+"' and password = '"+password+"';";
			}
			System.out.println(query);
			PreparedStatement pscheck = con.prepareStatement(query);
			ResultSet result = pscheck.executeQuery();
			
			if(result.next()) {
				ans = true;
			}
		}catch(Exception e) {
			System.out.println("ERROR in emailExists: "+e);
		}
		
		try {
			db.closeConnection(con);
		}catch(Exception e) {}
		
		return ans;
	}

//CHECK IF USER IS A MANAGER (AS OPPOSED TO A CUSTOMER REPRESENTATIVE)
	public boolean isManager(String username, String password) {
		ApplicationDB db = new ApplicationDB();	
		Connection con = db.getConnection();
		boolean isManager = false;
		try {
			String query = "";
			query = "select manager from manage_custrep where username = '"+username+"' and password = '"+password+"';";
			System.out.println(query);
			PreparedStatement pscheck = con.prepareStatement(query);
			ResultSet result = pscheck.executeQuery();
			
			if(result.next()) {
				String tmp = result.getString(1);
				System.out.println(tmp);
                if(tmp.equals("0")) {
                	isManager = false;
                }else {
                	isManager = true;
                }
				
			}
		}catch(Exception e) {
			System.out.println("ERROR in isManager: "+e);
		}
		
		try {
			db.closeConnection(con);
		}catch(Exception e) {}
		
		return isManager;
	}

//GET ALL INFO ON EMPLOYEES(IF TRUE) OR CUSTOMERS(IF FALSE)
	public Vector getInfo(boolean employee) {
		//if true, gets employee info, if false, gets customer info
		Vector ans = new Vector();
		ApplicationDB db = new ApplicationDB();	
		Connection con = db.getConnection();
		
		try {
			String query = "";
			if(employee) {
				query = "select ssn, firstName, lastName, phone, address, city, state, zipcode,  startDate, hourlyRate from employees;";
			}else {
				query = "select accountNumber, firstName, lastName, phone, address, city, state, zipCode, email, preferences, createDate, password, creditCard from customer";
			}
			System.out.println(query);
			PreparedStatement pscheck = con.prepareStatement(query);
			ResultSet result = pscheck.executeQuery();
			
			
			if(employee) {
				while(result.next()) {
					String tmp = result.getString(1);
					if(tmp==null) tmp = "";
					
					String xml = "<ssn>"+tmp+"</ssn>";
					
					tmp = result.getString(2);
					if(tmp==null) tmp = "";
					
					xml = xml + "<first>"+tmp+"</first>";
					
					tmp = result.getString(3);
					if(tmp==null) tmp = "";
					
					xml = xml + "<last>"+tmp+"</last>";
					
					tmp = result.getString(4);
					if(tmp==null) tmp = "";
					
					xml = xml + "<phone>"+tmp+"</phone>";
					
					tmp = result.getString(5);
					if(tmp==null) tmp = "";
					
					xml = xml + "<address>"+tmp+"</address>";
					
					tmp = result.getString(6);
					if(tmp==null) tmp = "";
					
					xml = xml + "<city>"+tmp+"</city>";
					
					tmp = result.getString(7);
					if(tmp==null) tmp = "";
					
					xml = xml + "<state>"+tmp+"</state>";
					
					tmp = result.getString(8);
					if(tmp==null) tmp = "";
					
					xml = xml + "<zip>"+tmp+"</zip>";
					
					tmp = result.getString(9);
					if(tmp==null) tmp = "";
					
					xml = xml + "<start>"+tmp+"</start>";
					
					tmp = result.getString(10);
					if(tmp==null) tmp = "";
					
					xml = xml + "<hourlyRate>"+tmp+"</hourlyRate>";
					
					ans.addElement(xml);
					
				}
			}else {
				while(result.next()) {
					String tmp = result.getString(1);
					if(tmp==null) tmp = "";
					
					String xml = "<account>"+tmp+"</account>";
					
					tmp = result.getString(2);
					if(tmp==null) tmp = "";
					
					xml = xml + "<first>"+tmp+"</first>";
					
					tmp = result.getString(3);
					if(tmp==null) tmp = "";
					
					xml = xml + "<last>"+tmp+"</last>";
					
					tmp = result.getString(4);
					if(tmp==null) tmp = "";
					
					xml = xml + "<phone>"+tmp+"</phone>";
					
					tmp = result.getString(5);
					if(tmp==null) tmp = "";
					
					xml = xml + "<address>"+tmp+"</address>";
					
					tmp = result.getString(6);
					if(tmp==null) tmp = "";
					
					xml = xml + "<city>"+tmp+"</city>";
					
					tmp = result.getString(7);
					if(tmp==null) tmp = "";
					
					xml = xml + "<state>"+tmp+"</state>";
					
					tmp = result.getString(8);
					if(tmp==null) tmp = "";
					
					xml = xml + "<zip>"+tmp+"</zip>";
					
					tmp = result.getString(9);
					if(tmp==null) tmp = "";
					
					xml = xml + "<email>"+tmp+"</email>";
					
					tmp = result.getString(10);
					if(tmp==null) tmp = "";
					
					xml = xml + "<preferences>"+tmp+"</preferences>";
					
					tmp = result.getString(11);
					if(tmp==null) tmp = "";
					
					xml = xml + "<createDate>"+tmp+"</createDate>";
					
					tmp = result.getString(12);
					if(tmp==null) tmp = "";
					
					xml = xml + "<password>"+tmp+"</password>";
					
					tmp = result.getString(13);
					if(tmp==null) tmp = "";
					
					xml = xml + "<credit>"+tmp+"</credit>";
					
					ans.addElement(xml);
					
				}
				
				
			}
			
		}catch(Exception e) {
			System.out.println("ERROR in getEmployees: "+e);
		}
		
		try {
			db.closeConnection(con);
		}catch(Exception e) {}
		
		return ans;
	}
	
//DELETE EMPLOYEE/CUSTOMER
	public void deleteInfo(String id, boolean employee) {
		ApplicationDB db = new ApplicationDB();	
		Connection con = db.getConnection();
	    try {
	    	String query = "";
	    	if(employee) {
	    		query = "delete from employees where ssn = '"+id+"';";
	    	}else{
	    		query = "delete from customer where accountNumber = "+id+";";
	    	}
	    	System.out.println(query);
	    	PreparedStatement x = con.prepareStatement(query);
	    	x.execute();
	    	
	    }catch(Exception e) {
	    	System.out.println("ERROR in deleteInfo: "+e);
	    }
	    
	    try {
	    	db.closeConnection(con);
		}catch(Exception e) {}
		
	}
	
//ADD NEW EMPLOYEE
	public String addEmployee(String ssn, String first, String last, String address, String city, String state, String zip, String phone, String startdate, String hourlyrate) {
		String error = "";
	    ApplicationDB db = new ApplicationDB();	
		Connection con = db.getConnection();
	    try {
	    	
	    	String query = "insert into employees(ssn, firstName, lastName, address, city, state, zipcode, phone, startDate, hourlyRate) "
				+ "values ('"+ssn+"','"+first+"','"+last+"','"+address+"','"+city+"','"+state+"','"+zip+"','"+phone+"','"+startdate+"','"+hourlyrate+"');";
	    	System.out.println(query);
	    	PreparedStatement emp = con.prepareStatement(query);
	    	emp.execute();
	    	
	    }catch(Exception e) {
	    	System.out.println("ERROR in addEmployee: "+e);
	    	error = "Error in inserting employee.  Check format of what you are trying to input";
	    }
	    
	    try {
	    	db.closeConnection(con);
		}catch(Exception e) {}
	    
	    return error;
	}

//GET A SPECIFIC EMPLOYEES INFO
	public String employeeInfoSsn(String ssn) {
		//gets individual employee's info for edit page
		String ans = "";
		ApplicationDB db = new ApplicationDB();	
		Connection con = db.getConnection();
		
		try {
			String query = "";
			query = "select ssn, firstName, lastName, phone, address, city, state, zipcode,  startDate, hourlyRate from employees where ssn = '"+ssn+"';";
			
			System.out.println(query);
			PreparedStatement pscheck = con.prepareStatement(query);
			ResultSet result = pscheck.executeQuery();
			
			
			while(result.next()) {
				String tmp = result.getString(1);
				if(tmp==null) tmp = "";
				
				ans= "<ssn>"+tmp+"</ssn>";
				
				tmp = result.getString(2);
				if(tmp==null) tmp = "";
				
				ans = ans+ "<first>"+tmp+"</first>";
				
				tmp = result.getString(3);
				if(tmp==null) tmp = "";
				
				ans = ans + "<last>"+tmp+"</last>";
				
				tmp = result.getString(4);
				if(tmp==null) tmp = "";
				
				ans = ans + "<phone>"+tmp+"</phone>";
				
				tmp = result.getString(5);
				if(tmp==null) tmp = "";
				
				ans = ans + "<address>"+tmp+"</address>";
				
				tmp = result.getString(6);
				if(tmp==null) tmp = "";
				
				ans = ans + "<city>"+tmp+"</city>";
				
				tmp = result.getString(7);
				if(tmp==null) tmp = "";
				
				ans = ans + "<state>"+tmp+"</state>";
				
				tmp = result.getString(8);
				if(tmp==null) tmp = "";
				
				ans = ans + "<zip>"+tmp+"</zip>";
				
				tmp = result.getString(9);
				if(tmp==null) tmp = "";
				
				ans = ans + "<start>"+tmp+"</start>";
				
				tmp = result.getString(10);
				if(tmp==null) tmp = "";
				
				ans = ans + "<hourlyRate>"+tmp+"</hourlyRate>";
				
				
			}
			
			
		}catch(Exception e) {
			System.out.println("ERROR in employeeInfoSsn: "+e);
		}
		
		try {
			db.closeConnection(con);
		}catch(Exception e) {}
		
		return ans;
	}

//EDIT A SPECIFIC EMPLOYEE
	public String editEmployee(String oldssn, String newssn, String first, String last, String address, String city, String state, String zip, String phone, String startdate, String hourlyrate) {
		String error = "";
	    ApplicationDB db = new ApplicationDB();	
		Connection con = db.getConnection();
	    try {
	    	
	    	String query = "update employees set "
	    			+ "ssn='"+newssn+"', "
					+ "firstName='"+first+"', "
					+ "lastName='"+last+"', "
					+ "address='"+address+"', "
					+ "city='"+city+"', "
					+ "state='"+state+"', "
					+ "zipcode='"+zip+"', "
					+ "phone='"+phone+"', "
					+ "startDate='"+startdate+"', "
					+ "hourlyRate='"+hourlyrate+"' "
	    			+ "where ssn='"+oldssn+"'";
	    	System.out.println(query);
	    	PreparedStatement emp = con.prepareStatement(query);
	    	emp.execute();
	    	
	    }catch(Exception e) {
	    	System.out.println("ERROR in editEmployee: "+e);
	    	error = "Error in editing employee.  Check format of what you are trying to input";
	    }
	    
	    try {
	    	db.closeConnection(con);
		}catch(Exception e) {}
	    
	    return error;
	}

//EDIT A SPECIFIC CUSTOMER
	public String editCustomer(String accountNum, String first, String last, String address, String city, String state, String zip, String phone, String email, String preferences, String credit, String password) {
		String error = "";
	    ApplicationDB db = new ApplicationDB();	
		Connection con = db.getConnection();
	    try {
	    	
	    	String query = "update customer set "
	    			+ "phone='"+phone+"', "
	    			+ "email='"+email+"', "
					+ "firstName='"+first+"', "
					+ "lastName='"+last+"', "
					+ "address='"+address+"', "
					+ "city='"+city+"', "
					+ "state='"+state+"', "
					+ "zipcode='"+zip+"', "
					+ "preferences='"+preferences+"', "
					+ "password='"+password+"', "
					+ "creditCard='"+credit+"' "
	    			+ "where accountNumber ='"+accountNum+"'";
	    	System.out.println(query);
	    	PreparedStatement cust = con.prepareStatement(query);
	    	cust.execute();
	    	
	    }catch(Exception e) {
	    	System.out.println("ERROR in editCustomer: "+e);
	    	error = "Error in editing customer.  Check format of what you are trying to input";
	    }
	    
	    try {
	    	db.closeConnection(con);
		}catch(Exception e) {}
	    
	    return error;
	}

//GET A SPECIFIC CUSTOMERS INFO USING ACCOUNTNUM
	public String custInfoAccNum(String accountNum) {
		
		String ans = "";
		ApplicationDB db = new ApplicationDB();	
		Connection con = db.getConnection();
		
		try {
			String query = "";
			query = "select accountNumber, firstName, lastName, phone, email, address, city, state, zipCode, preferences, createDate, password, creditCard from customer where accountNumber= '"+accountNum+"';";
			System.out.println(query);
			PreparedStatement pscheck = con.prepareStatement(query);
			ResultSet result = pscheck.executeQuery();
			
			
			while(result.next()) {
				String tmp = result.getString(1);
				if(tmp==null) tmp = "";
				
				ans= "<accountNum>"+tmp+"</accountNum>";
				
				tmp = result.getString(2);
				if(tmp==null) tmp = "";
				
				ans = ans+ "<first>"+tmp+"</first>";
				
				tmp = result.getString(3);
				if(tmp==null) tmp = "";
				
				ans = ans + "<last>"+tmp+"</last>";
				
				tmp = result.getString(4);
				if(tmp==null) tmp = "";
				
				ans = ans + "<phone>"+tmp+"</phone>";
				
				tmp = result.getString(5);
				if(tmp==null) tmp = "";
				
				ans = ans + "<email>"+tmp+"</email>";
				
				tmp = result.getString(6);
				if(tmp==null) tmp = "";
				
				ans = ans + "<address>"+tmp+"</address>";
				
				tmp = result.getString(7);
				if(tmp==null) tmp = "";
				
				ans = ans + "<city>"+tmp+"</city>";
				
				tmp = result.getString(8);
				if(tmp==null) tmp = "";
				
				ans = ans + "<state>"+tmp+"</state>";
				
				tmp = result.getString(9);
				if(tmp==null) tmp = "";
				
				ans = ans + "<zip>"+tmp+"</zip>";
				
				tmp = result.getString(10);
				if(tmp==null) tmp = "";
				
				ans = ans + "<preferences>"+tmp+"</preferences>";
				
				tmp = result.getString(11);
				if(tmp==null) tmp = "";
				
				ans = ans + "<createDate>"+tmp+"</createDate>";
				
				tmp = result.getString(12);
				if(tmp==null) tmp = "";
				
				ans = ans + "<password>"+tmp+"</password>";
				
				tmp = result.getString(11);
				if(tmp==null) tmp = "";
				
				ans = ans + "<credit>"+tmp+"</credit>";
				
			}
			
			
		}catch(Exception e) {
			System.out.println("ERROR in  custInfoAccNum: "+e);
		}
		
		try {
			db.closeConnection(con);
		}catch(Exception e) {}
		
		return ans;
	}

//GET A SPECIFIC CUSTOMERS INFO USING EMAIL
	public String custInfoEmail(String email) {
		//gets individual employee's info for edit page
		String ans = "";
		ApplicationDB db = new ApplicationDB();	
		Connection con = db.getConnection();
		
		try {
			String query = "";
			query = "select accountNumber, firstName, lastName, phone, email, address, city, state, zipCode, preferences, createDate, password, creditCard from customer where email= '"+email+"';";
			System.out.println(query);
			PreparedStatement pscheck = con.prepareStatement(query);
			ResultSet result = pscheck.executeQuery();
			
			
			while(result.next()) {
				String tmp = result.getString(1);
				if(tmp==null) tmp = "";
				
				ans= "<accountNum>"+tmp+"</accountNum>";
				
				tmp = result.getString(2);
				if(tmp==null) tmp = "";
				
				ans = ans+ "<first>"+tmp+"</first>";
				
				tmp = result.getString(3);
				if(tmp==null) tmp = "";
				
				ans = ans + "<last>"+tmp+"</last>";
				
				tmp = result.getString(4);
				if(tmp==null) tmp = "";
				
				ans = ans + "<phone>"+tmp+"</phone>";
				
				tmp = result.getString(5);
				if(tmp==null) tmp = "";
				
				ans = ans + "<email>"+tmp+"</email>";
				
				tmp = result.getString(6);
				if(tmp==null) tmp = "";
				
				ans = ans + "<address>"+tmp+"</address>";
				
				tmp = result.getString(7);
				if(tmp==null) tmp = "";
				
				ans = ans + "<city>"+tmp+"</city>";
				
				tmp = result.getString(8);
				if(tmp==null) tmp = "";
				
				ans = ans + "<state>"+tmp+"</state>";
				
				tmp = result.getString(9);
				if(tmp==null) tmp = "";
				
				ans = ans + "<zip>"+tmp+"</zip>";
				
				tmp = result.getString(10);
				if(tmp==null) tmp = "";
				
				ans = ans + "<preferences>"+tmp+"</preferences>";
				
				tmp = result.getString(11);
				if(tmp==null) tmp = "";
				
				ans = ans + "<createDate>"+tmp+"</createDate>";
				
				tmp = result.getString(12);
				if(tmp==null) tmp = "";
				
				ans = ans + "<password>"+tmp+"</password>";
				
				tmp = result.getString(13);
				if(tmp==null) tmp = "";
				
				ans = ans + "<credit>"+tmp+"</credit>";
				
			}
			
			
		}catch(Exception e) {
			System.out.println("ERROR in  custInfoEmail: "+e);
		}
		
		try {
			db.closeConnection(con);
		}catch(Exception e) {}
		
		return ans;
	}

//GET ALL OF A USERS RESERVATIONS
	public Vector getMyReservations(String account) {
		Vector ans = new Vector();
		ApplicationDB db = new ApplicationDB();	
		Connection con = db.getConnection();
		
		try {
			String query = "";
			query = "select reservationNum, customerID, totalFare, dateCreated from reservation where customerID = "+account+";";
			System.out.println(query);
			PreparedStatement pscheck = con.prepareStatement(query);
			ResultSet result = pscheck.executeQuery();
			
			while(result.next()) {
				String tmp = result.getString(1);
				if(tmp==null) tmp = "";
				
				String xml = "<reservationNum>"+tmp+"</reservationNum>";
				
				tmp = result.getString(2);
				if(tmp==null) tmp = "";
				
				xml = xml + "<customerID>"+tmp+"</customerID>";
				
				tmp = result.getString(3);
				if(tmp==null) tmp = "";
				
				xml = xml + "<totalFare>"+tmp+"</totalFare>";
				
				tmp = result.getString(4);
				if(tmp==null) tmp = "";
				
				xml = xml + "<dateCreated>"+tmp+"</dateCreated>";
						
				ans.addElement(xml);
				
			}
			
			
		}catch(Exception e) {
			System.out.println("ERROR in getMyReservations: "+e);
		}
		
		try {
			db.closeConnection(con);
		}catch(Exception e) {}
		
		return ans;
	}

//GET ALL SECTIONS OF A RESERVATION ASSOCIATED WITH A RESERVATION NUMBER
	public Vector getLegsOfReservation(String reservationNum) {
		Vector ans = new Vector();
		ApplicationDB db = new ApplicationDB();	
		Connection con = db.getConnection();
		
		try {
			String query = "";
			query = "select reservationNum, flightNumber, airlineID, departDateTime, specialMeal, class, seatNum from reserve_flights where reservationNum = '"+reservationNum+"';";
			System.out.println(query);
			PreparedStatement pscheck = con.prepareStatement(query);
			ResultSet result = pscheck.executeQuery();
			
			while(result.next()) {
				String tmp = result.getString(1);
				if(tmp==null) tmp = "";
				
				String xml = "<reservationNum>"+tmp+"</reservationNum>";
				
				tmp = result.getString(2);
				if(tmp==null) tmp = "";
				
				xml = xml + "<flightNumber>"+tmp+"</flightNumber>";
				
				tmp = result.getString(3);
				if(tmp==null) tmp = "";
				
				xml = xml + "<airlineID>"+tmp+"</airlineID>";
				
				tmp = result.getString(4);
				if(tmp==null) tmp = "";
				
				xml = xml + "<departDateTime>"+tmp+"</departDateTime>";

				tmp = result.getString(5);
				if(tmp==null) tmp = "";
				
				xml = xml + "<specialMeal>"+tmp+"</specialMeal>";

				tmp = result.getString(6);
				if(tmp==null) tmp = "";
				
				xml = xml + "<class>"+tmp+"</class>";

				tmp = result.getString(7);
				if(tmp==null) tmp = "";
				
				xml = xml + "<seatNum>"+tmp+"</seatNum>";

				ans.addElement(xml);
				
			}
			
			
		}catch(Exception e) {
			System.out.println("ERROR in getLegs: "+e);
		}
		
		try {
			db.closeConnection(con);
		}catch(Exception e) {}
		
		return ans;
		
		
	}

//DELETE ENTIRE RESERVATION
	public void deleteReservation(String reservationNum) {
		ApplicationDB db = new ApplicationDB();	
		Connection con = db.getConnection();
	    try {
	    	String query = "";
	    	query = "delete from reservation where reservationNum = '"+reservationNum+"'";
	    	
	    	System.out.println(query);
	    	PreparedStatement x = con.prepareStatement(query);
	    	x.execute();
	    	
	    }catch(Exception e) {
	    	System.out.println("ERROR in deleteReservation: "+e);
	    }
	    
	    try {
	    	db.closeConnection(con);
		}catch(Exception e) {}
		
		
	}

//GET BEST SELLING FLIGHTS
	public Vector getBestSellers() {
		Vector ans = new Vector();
		ApplicationDB db = new ApplicationDB();	
		Connection con = db.getConnection();
		
		try {
			String query = "";
			//query = "select * from flights";
			query = "select f.flightNumber, f.airlineID, f.numberOfSeats, f.fareRestrict, f.lengthOfStayRestrict, " + 
					"f.advancedPurchase, f.fare, f.departPort, f.departTime, f.arrivePort, f.arriveTime, sum(fareApplied) " + 
					"from flights f, reserve_flights r " + 
					"where f.flightNumber = r.flightNumber " + 
					"and f.airlineID = r.airlineID " + 
					"group by f.flightNumber, f.airlineID " + 
					"order by sum(fareApplied) ";
		
			System.out.println(query);
			PreparedStatement pscheck = con.prepareStatement(query);
			ResultSet result = pscheck.executeQuery();
			
			while(result.next()) {
				String tmp = result.getString(1);
				if(tmp==null) tmp = "";
				
				String xml = "<flightNum>"+tmp+"</flightNum>";
				
				tmp = result.getString(2);
				if(tmp==null) tmp = "";
				
				xml = xml + "<airlineID>"+tmp+"</airlineID>";
				
				tmp = result.getString(3);
				if(tmp==null) tmp = "";
				
				xml = xml + "<numberOfSeats>"+tmp+"</numberOfSeats>";
				
				tmp = result.getString(4);
				if(tmp==null) tmp = "";
				
				xml = xml + "<fareRestrict>"+tmp+"</fareRestrict>";

				tmp = result.getString(5);
				if(tmp==null) tmp = "";
				
				xml = xml + "<lengthOfStayRestrict>"+tmp+"</lengthOfStayRestrict>";

				tmp = result.getString(6);
				if(tmp==null) tmp = "";
				
				xml = xml + "<advancedPurchase>"+tmp+"</advancedPurchase>";

				tmp = result.getString(7);
				if(tmp==null) tmp = "";
				
				xml = xml + "<fare>"+tmp+"</fare>";

				tmp = result.getString(8);
				if(tmp==null) tmp = "";
				
				xml = xml + "<departPort>"+tmp+"</departPort>";
				
				tmp = result.getString(9);
				if(tmp==null) tmp = "";
				
				xml = xml + "<departTime>"+tmp+"</departTime>";
				
				tmp = result.getString(10);
				if(tmp==null) tmp = "";
				
				xml = xml + "<arrivePort>"+tmp+"</arrivePort>";
				
				tmp = result.getString(11);
				if(tmp==null) tmp = "";
				
				xml = xml + "<arriveTime>"+tmp+"</arriveTime>";
				
				tmp = result.getString(12);
				if(tmp==null) tmp = "";
				
				xml = xml + "<sum>"+tmp+"</sum>";
				
				ans.addElement(xml);
				
			}
			
			
		}catch(Exception e) {
			System.out.println("ERROR in getBestSellers : "+e);
		}
		
		try {
			db.closeConnection(con);
		}catch(Exception e) {}
		
		return ans;
		
	}

//GET DAYS A FLIGHT FLYS ON
	public Vector getFlightDays(String flightNum, String airID) {
		Vector ans = new Vector();
		ApplicationDB db = new ApplicationDB();	
		Connection con = db.getConnection();
		
		try {
			String query = "";
			query = "select flightNumber, airlineID, dayOfWeek from flightDays where flightNumber = '"+flightNum+"' and airlineID = '"+airID+"'";
			System.out.println(query);
			PreparedStatement pscheck = con.prepareStatement(query);
			ResultSet result = pscheck.executeQuery();
			
			while(result.next()) {
				String tmp = result.getString(3);
				if(tmp==null) tmp = "";
								
				ans.addElement(tmp);
				
			}
			
			
		}catch(Exception e) {
			System.out.println("ERROR in getFlightDays: "+e);
		}
		
		try {
			db.closeConnection(con);
		}catch(Exception e) {}
		
		return ans;
		
	}

//GET AMOUNT OF SEATS LEFT ON A FLIGHT
	public String seatsLeft(String flightNum,String airID, String departDateTime) {
		String ans = "0";
		ApplicationDB db = new ApplicationDB();	
		Connection con = db.getConnection();
		
		try {
			String query = "";
			query = "select (numberOfSeats - (select count(*) from reserve_flights where flightNumber = '"+flightNum+"' and airlineID = '"+airID+"' and departDateTime = '"+departDateTime+"')) " + 
					"from flights where flightNumber = '"+flightNum+"' and airlineID = '"+airID+"'";
			System.out.println(query);
			PreparedStatement pscheck = con.prepareStatement(query);
			ResultSet result = pscheck.executeQuery();
			
			while(result.next()) {
				
				ans = result.getString(1);
				
			}
			
			
		}catch(Exception e) {
			System.out.println("ERROR in getSeatsLeft : "+e);
		}
		
		try {
			db.closeConnection(con);
		}catch(Exception e) {}
		
		return ans;
	}

//GET ALL AIRPORTS 
	public Vector getAirports() {
		Vector ans = new Vector();
		ApplicationDB db = new ApplicationDB();	
		Connection con = db.getConnection();
		
		try {
			String query = "";
			query = "select portName, airportID from airport";
			System.out.println(query);
			PreparedStatement pscheck = con.prepareStatement(query);
			ResultSet result = pscheck.executeQuery();
			
			while(result.next()) {
				String tmp = result.getString(1);
				if(tmp==null) tmp = "";
				
				String xml = "<portName>"+tmp+"</portName>";
				
				tmp = result.getString(2);
				if(tmp==null) tmp = "";
				
				xml = xml + "<airportID>"+tmp+"</airportID>";
				
				ans.addElement(xml);
				
			}
			
			
		}catch(Exception e) {
			System.out.println("ERROR in getAirports : "+e);
		}
		
		try {
			db.closeConnection(con);
		}catch(Exception e) {}
		return ans;
	}

//GET ALL AIRPORT CITIES
	public Vector getAirportCities() {
		Vector ans = new Vector();
		ApplicationDB db = new ApplicationDB();	
		Connection con = db.getConnection();
		
		try {
			String query = "";
			query = "select distinct city from airport";
			System.out.println(query);
			PreparedStatement pscheck = con.prepareStatement(query);
			ResultSet result = pscheck.executeQuery();
			
			while(result.next()) {
				String tmp = result.getString(1);
				if(tmp==null) tmp = "";
				
				String xml = "<portCity>"+tmp+"</portCity>";
				
				ans.addElement(xml);
				
			}
			
			
		}catch(Exception e) {
			System.out.println("ERROR in getAirportCities : "+e);
		}
		
		try {
			db.closeConnection(con);
		}catch(Exception e) {}
		return ans;
	}

//GET SEARCH RESULTS BY AIRPORT ID
	public Vector getFlightsOneWay(String from, String to, String departDate, String timemin, String timemax, String passengers) throws ParseException {
		Vector ans = new Vector();
		ApplicationDB db = new ApplicationDB();	
		Connection con = db.getConnection();
	    
		
		//String sDate1="31/12/1998";  
	    Date date=new SimpleDateFormat("yyyy-MM-dd").parse(departDate);  
	    
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
	    int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
		System.out.println(departDate + ":  " +dayOfWeek);
		
		try {
			String query = "";
			query = "select f.flightNumber, f.airlineID, f.numberOfSeats, f.fareRestrict, f.lengthOfStayRestrict, f.advancedPurchase, f.fare, f.departPort, f.departTime, f.arrivePort, f.arriveTime, d.dayOfWeek, f.apdiscount " +
					"from flights f, flightDays d  " + 
					"where f.flightNumber = d.flightNumber " + 
					"and f.airlineID = d.airlineID  " + 
					"and f.departPort = '"+from+"'  " + 
					"and f.arrivePort = '"+to+"'  " + 
					"and d.dayOfWeek = '"+dayOfWeek+"' " + 
					"and f.departTime > '"+timemin+"' " + 
					"and f.departTime < '"+timemax+"';";
			System.out.println(query);
			PreparedStatement pscheck = con.prepareStatement(query);
			ResultSet result = pscheck.executeQuery();
			
			while(result.next()) {
				String tmp = result.getString(1);
				if(tmp==null) tmp = "";
				
				String xml = "<flightNumber>"+tmp+"</flightNumber>";
				
				tmp = result.getString(2);
				if(tmp==null) tmp = "";
				
				xml = xml + "<airlineID>"+tmp+"</airlineID>";
				
				tmp = result.getString(3);
				if(tmp==null) tmp = "";
				
				xml = xml + "<numberOfSeats>"+tmp+"</numberOfSeats>";
				
				tmp = result.getString(4);
				if(tmp==null) tmp = "";
				
				xml = xml + "<fareRestrict>"+tmp+"</fareRestrict>";
				
				tmp = result.getString(5);
				if(tmp==null) tmp = "";
				
				xml = xml + "<lengthOfStay>"+tmp+"</lengthOfStay>";
				
				tmp = result.getString(6);
				if(tmp==null) tmp = "";
				
				xml = xml + "<advancedPurchase>"+tmp+"</advancedPurchase>";
				
				tmp = result.getString(7);
				if(tmp==null) tmp = "";
				
				xml = xml + "<fare>"+tmp+"</fare>";
				
				tmp = result.getString(8);
				if(tmp==null) tmp = "";
				
				xml = xml + "<departPort>"+tmp+"</departPort>";
				
				tmp = result.getString(9);
				if(tmp==null) tmp = "";
				
				xml = xml + "<departTime>"+tmp+"</departTime>";
				
				tmp = result.getString(10);
				if(tmp==null) tmp = "";
				
				xml = xml + "<arrivePort>"+tmp+"</arrivePort>";
				
				tmp = result.getString(11);
				if(tmp==null) tmp = "";
				
				xml = xml + "<arriveTime>"+tmp+"</arriveTime>";
				
				tmp = result.getString(12);
				if(tmp==null) tmp = "";
				
				xml = xml + "<dayOfWeek>"+tmp+"</dayOfWeek>";
				
				tmp = result.getString(13);
				if(tmp==null) tmp = "";
				
				xml = xml + "<apdiscount>"+tmp+"</apdiscount>";
				
				ans.addElement(xml);
				
			}
			
			
		}catch(Exception e) {
			System.out.println("ERROR in getSearchResults : "+e);
		}
		
		try {
			db.closeConnection(con);
		}catch(Exception e) {}
		return ans;
		
		
		
	}

//GET SEARCH RESULTS BY AIRPORT CITY
	public Vector getFlightsOneWayByCity(String from, String to, String departDate, String timemin, String timemax, String passengers) throws ParseException {
		Vector ans = new Vector();
		ApplicationDB db = new ApplicationDB();	
		Connection con = db.getConnection();
	    
	    Date date=new SimpleDateFormat("yyyy-MM-dd").parse(departDate);  
	    
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
	    int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
		System.out.println(departDate + ":  " +dayOfWeek);
		
		try {
			String query = "";
			query = "select f.flightNumber, f.airlineID, f.numberOfSeats, f.fareRestrict, f.lengthOfStayRestrict, f.advancedPurchase, f.fare, f.departPort, f.departTime, f.arrivePort, f.arriveTime, d.dayOfWeek, f.apdiscount " +
					"from flights f, flightDays d  " + 
					"where f.flightNumber = d.flightNumber " + 
					"and f.airlineID = d.airlineID  " + 
					//"and f.departPort = '"+from+"'  " + 
					"and f.departPort in (select a.airportID from airport a where a.city = '"+from+"')" +
					//"and f.arrivePort = '"+to+"'  " + 
					"and f.arrivePort in (select a.airportID from airport a where a.city = '"+to+"')" +
					"and d.dayOfWeek = '"+dayOfWeek+"' " + 
					"and f.departTime > '"+timemin+"' " + 
					"and f.departTime < '"+timemax+"';";
			System.out.println(query);
			PreparedStatement pscheck = con.prepareStatement(query);
			ResultSet result = pscheck.executeQuery();
			
			while(result.next()) {
				String tmp = result.getString(1);
				if(tmp==null) tmp = "";
				
				String xml = "<flightNumber>"+tmp+"</flightNumber>";
				
				tmp = result.getString(2);
				if(tmp==null) tmp = "";
				
				xml = xml + "<airlineID>"+tmp+"</airlineID>";
				
				tmp = result.getString(3);
				if(tmp==null) tmp = "";
				
				xml = xml + "<numberOfSeats>"+tmp+"</numberOfSeats>";
				
				tmp = result.getString(4);
				if(tmp==null) tmp = "";
				
				xml = xml + "<fareRestrict>"+tmp+"</fareRestrict>";
				
				tmp = result.getString(5);
				if(tmp==null) tmp = "";
				
				xml = xml + "<lengthOfStay>"+tmp+"</lengthOfStay>";
				
				tmp = result.getString(6);
				if(tmp==null) tmp = "";
				
				xml = xml + "<advancedPurchase>"+tmp+"</advancedPurchase>";
				
				tmp = result.getString(7);
				if(tmp==null) tmp = "";
				
				xml = xml + "<fare>"+tmp+"</fare>";
				
				tmp = result.getString(8);
				if(tmp==null) tmp = "";
				
				xml = xml + "<departPort>"+tmp+"</departPort>";
				
				tmp = result.getString(9);
				if(tmp==null) tmp = "";
				
				xml = xml + "<departTime>"+tmp+"</departTime>";
				
				tmp = result.getString(10);
				if(tmp==null) tmp = "";
				
				xml = xml + "<arrivePort>"+tmp+"</arrivePort>";
				
				tmp = result.getString(11);
				if(tmp==null) tmp = "";
				
				xml = xml + "<arriveTime>"+tmp+"</arriveTime>";
				
				tmp = result.getString(12);
				if(tmp==null) tmp = "";
				
				xml = xml + "<dayOfWeek>"+tmp+"</dayOfWeek>";
				
				tmp = result.getString(13);
				if(tmp==null) tmp = "";
				
				xml = xml + "<apdiscount>"+tmp+"</apdiscount>";
				
				ans.addElement(xml);
				
			}
			
			
		}catch(Exception e) {
			System.out.println("ERROR in getSearchResults : "+e);
		}
		
		try {
			db.closeConnection(con);
		}catch(Exception e) {}
		return ans;
		
		
		
	}

//GET SEARCH RESULTS BY AIRPORT ID AND FLEXIBLE DATE
	public Vector getFlightsOneWayFlex(String from, String to, String departDate1, String departDate2, String timemin, String timemax, String passengers) throws ParseException {
		Vector ans = new Vector();
		ApplicationDB db = new ApplicationDB();	
		Connection con = db.getConnection();
	    
		
		//String sDate1="31/12/1998";
		Date date1=new SimpleDateFormat("yyyy-MM-dd").parse(departDate1);  
	    Date date2=new SimpleDateFormat("yyyy-MM-dd").parse(departDate2);  
	    
		Calendar calendar1 = Calendar.getInstance();
		calendar1.setTime(date1);
		
		Calendar calendar2 = Calendar.getInstance();
		calendar2.setTime(date2);
				
	    int dayOfWeek1 = calendar1.get(Calendar.DAY_OF_WEEK);
		System.out.println(departDate1 + ":  " +dayOfWeek1);
		
		int dayOfWeek2 = calendar2.get(Calendar.DAY_OF_WEEK);
		System.out.println(departDate2 + ":  " +dayOfWeek2);
		
		String days = "";
		if(dayOfWeek1 < dayOfWeek2) {
			while(dayOfWeek1 <= dayOfWeek2) {
				days = days+"'"+dayOfWeek1+"'";
				dayOfWeek1++;
				
				if(dayOfWeek1 < dayOfWeek2) {
					days = days + ",";
				}
			}
		}else if(dayOfWeek1 > dayOfWeek2) {
			while(dayOfWeek1 <= 7) {
				days = days + "'"+dayOfWeek1+"',";
			}
			dayOfWeek1 = 1;
			while(dayOfWeek1 <= dayOfWeek2) {
				days = days + "'"+dayOfWeek1+"'";
				dayOfWeek1++;
				
				if(dayOfWeek1 < dayOfWeek2) {
					days = days+ ",";
				}
			}
		}else{
			if(departDate1 == departDate2) {
				days = "'"+dayOfWeek1+"'";
			}else{
				days = "'1','2','3','4','5','6','7'";
			}
		}
		
		
		
		try {
			String query = "";
			query = "select f.flightNumber, f.airlineID, f.numberOfSeats, f.fareRestrict, f.lengthOfStayRestrict, f.advancedPurchase, f.fare, f.departPort, f.departTime, f.arrivePort, f.arriveTime, d.dayOfWeek " +
					"from flights f, flightDays d  " + 
					"where f.flightNumber = d.flightNumber " + 
					"and f.airlineID = d.airlineID  " + 
					"and f.departPort = '"+from+"'  " + 
					"and f.arrivePort = '"+to+"'  " + 
					"and d.dayOfWeek in ("+days+") " + 
					"and f.departTime > '"+timemin+"' " + 
					"and f.departTime < '"+timemax+"';";
			System.out.println(query);
			PreparedStatement pscheck = con.prepareStatement(query);
			ResultSet result = pscheck.executeQuery();
			
			while(result.next()) {
				String tmp = result.getString(1);
				if(tmp==null) tmp = "";
				
				String xml = "<flightNumber>"+tmp+"</flightNumber>";
				
				tmp = result.getString(2);
				if(tmp==null) tmp = "";
				
				xml = xml + "<airlineID>"+tmp+"</airlineID>";
				
				tmp = result.getString(3);
				if(tmp==null) tmp = "";
				
				xml = xml + "<numberOfSeats>"+tmp+"</numberOfSeats>";
				
				tmp = result.getString(4);
				if(tmp==null) tmp = "";
				
				xml = xml + "<fareRestrict>"+tmp+"</fareRestrict>";
				
				tmp = result.getString(5);
				if(tmp==null) tmp = "";
				
				xml = xml + "<lengthOfStay>"+tmp+"</lengthOfStay>";
				
				tmp = result.getString(6);
				if(tmp==null) tmp = "";
				
				xml = xml + "<advancedPurchase>"+tmp+"</advancedPurchase>";
				
				tmp = result.getString(7);
				if(tmp==null) tmp = "";
				
				xml = xml + "<fare>"+tmp+"</fare>";
				
				tmp = result.getString(8);
				if(tmp==null) tmp = "";
				
				xml = xml + "<departPort>"+tmp+"</departPort>";
				
				tmp = result.getString(9);
				if(tmp==null) tmp = "";
				
				xml = xml + "<departTime>"+tmp+"</departTime>";
				
				tmp = result.getString(10);
				if(tmp==null) tmp = "";
				
				xml = xml + "<arrivePort>"+tmp+"</arrivePort>";
				
				tmp = result.getString(11);
				if(tmp==null) tmp = "";
				
				xml = xml + "<arriveTime>"+tmp+"</arriveTime>";
				
				tmp = result.getString(12);
				if(tmp==null) tmp = "";
				
				xml = xml + "<dayOfWeek>"+tmp+"</dayOfWeek>";
				
				ans.addElement(xml);
				
			}
			
			
		}catch(Exception e) {
			System.out.println("ERROR in getSearchResults : "+e);
		}
		
		try {
			db.closeConnection(con);
		}catch(Exception e) {}
		return ans;
		
		
		
	}

//GET SEARCH RESULTS BY AIRPORT CITY AND FLEXIBLE DATE
	public Vector getFlightsOneWayFlexByCity(String from, String to, String departDate1, String departDate2, String timemin, String timemax, String passengers) throws ParseException {
		Vector ans = new Vector();
		ApplicationDB db = new ApplicationDB();	
		Connection con = db.getConnection();
	    
		
		//String sDate1="31/12/1998";
		Date date1=new SimpleDateFormat("yyyy-MM-dd").parse(departDate1);  
	    Date date2=new SimpleDateFormat("yyyy-MM-dd").parse(departDate2);  
	    
		Calendar calendar1 = Calendar.getInstance();
		calendar1.setTime(date1);
		
		Calendar calendar2 = Calendar.getInstance();
		calendar2.setTime(date2);
				
	    int dayOfWeek1 = calendar1.get(Calendar.DAY_OF_WEEK);
		System.out.println(departDate1 + ":  " +dayOfWeek1);
		
		int dayOfWeek2 = calendar2.get(Calendar.DAY_OF_WEEK);
		System.out.println(departDate2 + ":  " +dayOfWeek2);
		
		String days = "";
		if(dayOfWeek1 < dayOfWeek2) {
			while(dayOfWeek1 <= dayOfWeek2) {
				days = days+"'"+dayOfWeek1+"'";
				dayOfWeek1++;
				
				if(dayOfWeek1 < dayOfWeek2) {
					days = days + ",";
				}
			}
		}else if(dayOfWeek1 > dayOfWeek2) {
			while(dayOfWeek1 <= 7) {
				days = days + "'"+dayOfWeek1+"',";
			}
			dayOfWeek1 = 1;
			while(dayOfWeek1 <= dayOfWeek2) {
				days = days + "'"+dayOfWeek1+"'";
				dayOfWeek1++;
				
				if(dayOfWeek1 < dayOfWeek2) {
					days = days+ ",";
				}
			}
		}else{
			if(departDate1 == departDate2) {
				days = "'"+dayOfWeek1+"'";
			}else{
				days = "'1','2','3','4','5','6','7'";
			}
		}
		
		
		
		try {
			String query = "";
			query = "select f.flightNumber, f.airlineID, f.numberOfSeats, f.fareRestrict, f.lengthOfStayRestrict, f.advancedPurchase, f.fare, f.departPort, f.departTime, f.arrivePort, f.arriveTime, d.dayOfWeek " +
					"from flights f, flightDays d  " + 
					"where f.flightNumber = d.flightNumber " + 
					"and f.airlineID = d.airlineID  " + 
					//"and f.departPort = '"+from+"'  " + 
					"and f.departPort in (select a.airportID from airport a where a.city = '"+from+"')" +
					//"and f.arrivePort = '"+to+"'  " + 
					"and f.arrivePort in (select a.airportID from airport a where a.city = '"+to+"')" +
					"and d.dayOfWeek in ("+days+") " + 
					"and f.departTime > '"+timemin+"' " + 
					"and f.departTime < '"+timemax+"';";
			System.out.println(query);
			PreparedStatement pscheck = con.prepareStatement(query);
			ResultSet result = pscheck.executeQuery();
			
			while(result.next()) {
				String tmp = result.getString(1);
				if(tmp==null) tmp = "";
				
				String xml = "<flightNumber>"+tmp+"</flightNumber>";
				
				tmp = result.getString(2);
				if(tmp==null) tmp = "";
				
				xml = xml + "<airlineID>"+tmp+"</airlineID>";
				
				tmp = result.getString(3);
				if(tmp==null) tmp = "";
				
				xml = xml + "<numberOfSeats>"+tmp+"</numberOfSeats>";
				
				tmp = result.getString(4);
				if(tmp==null) tmp = "";
				
				xml = xml + "<fareRestrict>"+tmp+"</fareRestrict>";
				
				tmp = result.getString(5);
				if(tmp==null) tmp = "";
				
				xml = xml + "<lengthOfStay>"+tmp+"</lengthOfStay>";
				
				tmp = result.getString(6);
				if(tmp==null) tmp = "";
				
				xml = xml + "<advancedPurchase>"+tmp+"</advancedPurchase>";
				
				tmp = result.getString(7);
				if(tmp==null) tmp = "";
				
				xml = xml + "<fare>"+tmp+"</fare>";
				
				tmp = result.getString(8);
				if(tmp==null) tmp = "";
				
				xml = xml + "<departPort>"+tmp+"</departPort>";
				
				tmp = result.getString(9);
				if(tmp==null) tmp = "";
				
				xml = xml + "<departTime>"+tmp+"</departTime>";
				
				tmp = result.getString(10);
				if(tmp==null) tmp = "";
				
				xml = xml + "<arrivePort>"+tmp+"</arrivePort>";
				
				tmp = result.getString(11);
				if(tmp==null) tmp = "";
				
				xml = xml + "<arriveTime>"+tmp+"</arriveTime>";
				
				tmp = result.getString(12);
				if(tmp==null) tmp = "";
				
				xml = xml + "<dayOfWeek>"+tmp+"</dayOfWeek>";
				
				ans.addElement(xml);
				
			}
			
			
		}catch(Exception e) {
			System.out.println("ERROR in getSearchResults : "+e);
		}
		
		try {
			db.closeConnection(con);
		}catch(Exception e) {}
		return ans;
		
		
		
	}

//ADD A ONE WAY RESERVATION AS A CUSTOMER
	public String addReservationSelf(String flightNum, String airlineID, String departDateTime, String meal, String seatClass, String seatNum, String fare, String customerID) {
		Calendar calendar = Calendar.getInstance();
	    java.sql.Date create_date = new java.sql.Date(calendar.getTime().getTime());
	    System.out.println(create_date);
	    
	    String error = "";
	    ApplicationDB db = new ApplicationDB();	
		Connection con = db.getConnection();
		
		if(seatNum==null || seatNum.equals("null") || seatNum.equals("")) {
			seatNum="0";
		}
		
	    try {
	    	//INSERT INITIAL RESERVATION VALUES
	    	String query = "insert into reservation(customerID, dateCreated) values("+customerID+",'"+create_date+"')";
	    	System.out.println(query);
	    	PreparedStatement customer = con.prepareStatement(query);
	    	customer.execute();
	    	
	    }catch(Exception e) {
	    	System.out.println("ERROR in createReserve1: "+e);
	    	error = "Error in creating reservation.  Check format of what you are trying to input";
	    }
	    
	    String reserveNum = "";
	    
	    try {
	    	//GET RESERVATION NUMBER
	    	String query = "select max(reservationNum) from reservation;";
	    	System.out.println(query);
	    	PreparedStatement customer = con.prepareStatement(query);
	    	ResultSet result = customer.executeQuery();
	    	while(result.next()) {
	    		reserveNum = result.getString(1);
	    	}	
				
	    }catch(Exception e) {
	    	System.out.println("ERROR in createReserve2: "+e);
	    	error = "Error in creating reservation.  Check format of what you are trying to input";
	    }
	    
	    try {
	    	String query = "insert into reserve_flights values('"+reserveNum+"','"+flightNum+"','"+airlineID+"','"+departDateTime+"','"+meal+"','"+seatClass+"','"+seatNum+"','"+fare+"')";
	    	System.out.println(query);
	    	PreparedStatement customer = con.prepareStatement(query);
	    	customer.execute();
	    	
	    }catch(Exception e) {
	    	System.out.println("ERROR in createReserve3: "+e);
	    	error = "Error in creating reservation.  Check format of what you are trying to input";
	    }
	    
	    
	    
	    try {
	    	//UPDATE RESERVATION TABLE
	    	String query = "update reservation " + 
	    			"set totalFare = ((select sum(fareApplied) from reserve_flights where reservationNum = '"+reserveNum+"')*1.10), " + 
	    			"	booking_fee = ((select sum(fareApplied) from reserve_flights where reservationNum = '"+reserveNum+"')*.1) " + 
	    			"where reservationNum = '"+reserveNum+"';";
	    	System.out.println(query);
	    	PreparedStatement customer = con.prepareStatement(query);
	    	customer.execute();
	    }catch(Exception e) {
	    	System.out.println("ERROR in createReserve4: "+e);
	    	error = "Error in creating reservation.  Check format of what you are trying to input";
	    }
	    
	    try {
	    	db.closeConnection(con);
		}catch(Exception e) {}
		return reserveNum;
	}
	
//ADD A ROUND TRIP RESERVATION AS A CUSTOMER
	public String addReservationSelfRoundtrip(String flightNum1, String flightNum2, String airline1, String airline2, String departDateTime1, String departDateTime2, String meal1, String meal2, String seatClass1, String seatClass2, String seatNum1, String seatNum2, String fare1, String fare2, String customerID) {
		Calendar calendar = Calendar.getInstance();
	    java.sql.Date create_date = new java.sql.Date(calendar.getTime().getTime());
	    System.out.println(create_date);
	    
	    String error = "";
	    ApplicationDB db = new ApplicationDB();	
		Connection con = db.getConnection();
		
		if(seatNum1==null || seatNum1.equals("null") || seatNum1.equals("")) {
			seatNum1="0";
		}
		if(seatNum2==null || seatNum2.equals("null") || seatNum2.equals("")) {
			seatNum2="0";
		}
		
	    try {
	    	//INSERT INITIAL RESERVATION VALUES
	    	String query = "insert into reservation(customerID, dateCreated) values("+customerID+",'"+create_date+"')";
	    	System.out.println(query);
	    	PreparedStatement customer = con.prepareStatement(query);
	    	customer.execute();
	    	
	    }catch(Exception e) {
	    	System.out.println("ERROR in createReserve1: "+e);
	    	error = "Error in creating reservation.  Check format of what you are trying to input";
	    }
	    
	    String reserveNum = "";
	    
	    try {
	    	//GET RESERVATION NUMBER
	    	String query = "select max(reservationNum) from reservation;";
	    	System.out.println(query);
	    	PreparedStatement customer = con.prepareStatement(query);
	    	ResultSet result = customer.executeQuery();
	    	while(result.next()) {
	    		reserveNum = result.getString(1);
	    	}	
				
	    }catch(Exception e) {
	    	System.out.println("ERROR in createReserve2: "+e);
	    	error = "Error in creating reservation.  Check format of what you are trying to input";
	    }
	    
	    try {
	    	String query = "insert into reserve_flights values('"+reserveNum+"','"+flightNum1+"','"+airline1+"','"+departDateTime1+"','"+meal1+"','"+seatClass1+"','"+seatNum1+"','"+fare1+"')";
	    	System.out.println(query);
	    	PreparedStatement customer = con.prepareStatement(query);
	    	customer.execute();
	    	
	    }catch(Exception e) {
	    	System.out.println("ERROR in createReserve3: "+e);
	    	error = "Error in creating reservation.  Check format of what you are trying to input";
	    }
	    
	    try {
	    	String query = "insert into reserve_flights values('"+reserveNum+"','"+flightNum2+"','"+airline2+"','"+departDateTime2+"','"+meal2+"','"+seatClass2+"','"+seatNum2+"','"+fare2+"')";
	    	System.out.println(query);
	    	PreparedStatement customer = con.prepareStatement(query);
	    	customer.execute();
	    	
	    }catch(Exception e) {
	    	System.out.println("ERROR in createReserve3: "+e);
	    	error = "Error in creating reservation.  Check format of what you are trying to input";
	    }
	    
	    
	    
	    try {
	    	//UPDATE RESERVATION TABLE
	    	String query = "update reservation " + 
	    			"set totalFare = ((select sum(fareApplied) from reserve_flights where reservationNum = '"+reserveNum+"')*1.10), " + 
	    			"	booking_fee = ((select sum(fareApplied) from reserve_flights where reservationNum = '"+reserveNum+"')*.1) " + 
	    			"where reservationNum = '"+reserveNum+"';";
	    	System.out.println(query);
	    	PreparedStatement customer = con.prepareStatement(query);
	    	customer.execute();
	    }catch(Exception e) {
	    	System.out.println("ERROR in createReserve4: "+e);
	    	error = "Error in creating reservation.  Check format of what you are trying to input";
	    }
	    
	    try {
	    	db.closeConnection(con);
		}catch(Exception e) {}
		return reserveNum;
	}
	
//ADD A MULTI CITY RESERVATION AS A CUSTOMER
	public String addReservationSelfMulti(String flightNum1, String flightNum2, String flightNum3, String flightNum4, String airline1, String airline2, String airline3, String airline4, String dateTime1, String dateTime2, String dateTime3, String dateTime4, String meal1, String meal2, String meal3, String meal4, String seatClass1, String seatClass2, String seatClass3, String seatClass4, String seatNum1, String seatNum2, String seatNum3, String seatNum4, String fare1, String fare2, String fare3, String fare4, String customerID) {
		Calendar calendar = Calendar.getInstance();
	    java.sql.Date create_date = new java.sql.Date(calendar.getTime().getTime());
	    System.out.println(create_date);
	    
	    String error = "";
	    ApplicationDB db = new ApplicationDB();	
		Connection con = db.getConnection();
		
		if(seatNum1==null || seatNum1.equals("null") || seatNum1.equals("")) {
			seatNum1="0";
		}
		if(seatNum2==null || seatNum2.equals("null") || seatNum2.equals("")) {
			seatNum2="0";
		}
		if(seatNum3==null || seatNum3.equals("null") || seatNum3.equals("")) {
			seatNum3="0";
		}
		if(seatNum4==null || seatNum4.equals("null") || seatNum4.equals("")) {
			seatNum4="0";
		}
		
		
	    try {
	    	//INSERT INITIAL RESERVATION VALUES
	    	String query = "insert into reservation(customerID, dateCreated) values("+customerID+",'"+create_date+"')";
	    	System.out.println(query);
	    	PreparedStatement customer = con.prepareStatement(query);
	    	customer.execute();
	    	
	    }catch(Exception e) {
	    	System.out.println("ERROR in createReserve1: "+e);
	    	error = "Error in creating reservation.  Check format of what you are trying to input";
	    }
	    
	    String reserveNum = "";
	    
	    try {
	    	//GET RESERVATION NUMBER
	    	String query = "select max(reservationNum) from reservation;";
	    	System.out.println(query);
	    	PreparedStatement customer = con.prepareStatement(query);
	    	ResultSet result = customer.executeQuery();
	    	while(result.next()) {
	    		reserveNum = result.getString(1);
	    	}	
				
	    }catch(Exception e) {
	    	System.out.println("ERROR in createReserve2: "+e);
	    	error = "Error in creating reservation.  Check format of what you are trying to input";
	    }
	    
	    //INSERT LEGS
	    try {
	    	String query = "insert into reserve_flights values('"+reserveNum+"','"+flightNum1+"','"+airline1+"','"+dateTime1+"','"+meal1+"','"+seatClass1+"','"+seatNum1+"','"+fare1+"')";
	    	System.out.println(query);
	    	PreparedStatement customer = con.prepareStatement(query);
	    	customer.execute();
	    	
	    }catch(Exception e) {
	    	System.out.println("ERROR in createReserve3: "+e);
	    	error = "Error in creating reservation.  Check format of what you are trying to input";
	    }
	    
	    try {
	    	String query = "insert into reserve_flights values('"+reserveNum+"','"+flightNum2+"','"+airline2+"','"+dateTime2+"','"+meal2+"','"+seatClass2+"','"+seatNum2+"','"+fare2+"')";
	    	System.out.println(query);
	    	PreparedStatement customer = con.prepareStatement(query);
	    	customer.execute();
	    	
	    }catch(Exception e) {
	    	System.out.println("ERROR in createReserve3: "+e);
	    	error = "Error in creating reservation.  Check format of what you are trying to input";
	    }
	    
	    try {
	    	String query = "insert into reserve_flights values('"+reserveNum+"','"+flightNum3+"','"+airline3+"','"+dateTime3+"','"+meal3+"','"+seatClass3+"','"+seatNum3+"','"+fare3+"')";
	    	System.out.println(query);
	    	PreparedStatement customer = con.prepareStatement(query);
	    	customer.execute();
	    	
	    }catch(Exception e) {
	    	System.out.println("ERROR in createReserve3: "+e);
	    	error = "Error in creating reservation.  Check format of what you are trying to input";
	    }
	    
	    try {
	    	String query = "insert into reserve_flights values('"+reserveNum+"','"+flightNum4+"','"+airline4+"','"+dateTime4+"','"+meal4+"','"+seatClass4+"','"+seatNum4+"','"+fare4+"')";
	    	System.out.println(query);
	    	PreparedStatement customer = con.prepareStatement(query);
	    	customer.execute();
	    	
	    }catch(Exception e) {
	    	System.out.println("ERROR in createReserve3: "+e);
	    	error = "Error in creating reservation.  Check format of what you are trying to input";
	    }
	    
	    
	    
	    try {
	    	//UPDATE RESERVATION TABLE
	    	String query = "update reservation " + 
	    			"set totalFare = ((select sum(fareApplied) from reserve_flights where reservationNum = '"+reserveNum+"')*1.10), " + 
	    			"	booking_fee = ((select sum(fareApplied) from reserve_flights where reservationNum = '"+reserveNum+"')*.1) " + 
	    			"where reservationNum = '"+reserveNum+"';";
	    	System.out.println(query);
	    	PreparedStatement customer = con.prepareStatement(query);
	    	customer.execute();
	    }catch(Exception e) {
	    	System.out.println("ERROR in createReserve4: "+e);
	    	error = "Error in creating reservation.  Check format of what you are trying to input";
	    }
	    
	    try {
	    	db.closeConnection(con);
		}catch(Exception e) {}
		return reserveNum;
	}
	
//ADD A RESERVATION AS A CUSTOMER REPRESENTATIVE
	public String addReserveCustRep(String reservationNum, String customerID, String totalFare, String dateCreated, String booking_fee, String customer_rep){
	    
	    String error = "Successfully added";
	    ApplicationDB db = new ApplicationDB();	
		Connection con = db.getConnection();
		
	    try {
	    	
	    	String query = "insert into reservation "
				+ "values ('"+reservationNum+"','"+customerID+"','"+totalFare+"','"+dateCreated+"','"+booking_fee+"','"+customer_rep+"');";
	    	System.out.println(query);
	    	PreparedStatement customer = con.prepareStatement(query);
	    	customer.execute();
	    	
	    }catch(Exception e) {
	    	System.out.println("ERROR in addCustomer: "+e);
	    	error = "Error in adding reservation.  Check format of what you are trying to input";
	    }
	    
	    try {
	    	db.closeConnection(con);
		}catch(Exception e) {}
		return error;
	}
	
//ADD A SECTION OF A RESERVATION AS A CUSTOMER REPRESENTATIVE 
	public String addReserveSectionCustRep(String reservationNum, String flightNumber, String airlineID, String departDateTime, String specialMeal, String seatclass, String seatNum, String fareApplied){
	    
	    String error = "Successfully added";
	    ApplicationDB db = new ApplicationDB();	
		Connection con = db.getConnection();
		
	    try {
	    	
	    	String query = "insert into reserve_flights "
				+ "values ('"+reservationNum+"','"+flightNumber+"','"+airlineID+"','"+departDateTime+"','"+specialMeal+"','"+seatclass+"','"+seatNum+"','"+fareApplied+"');";
	    	System.out.println(query);
	    	PreparedStatement customer = con.prepareStatement(query);
	    	customer.execute();
	    	
	    }catch(Exception e) {
	    	System.out.println("ERROR in addCustomer: "+e);
	    	error = "Error in adding reservation.  Check format of what you are trying to input";
	    }
	    
	    try {
	    	db.closeConnection(con);
		}catch(Exception e) {}
		return error;
	}
	
//GET AN INDIVIDUAL FLIGHTS INFO 
	public String getFlightInfo(String flightNum, String airlineID) {
		String ans = "";
		ApplicationDB db = new ApplicationDB();	
		Connection con = db.getConnection();
		
		try {
			String query = "";
			query = "select * from flights where flightNumber = '"+flightNum+"' and airlineID='"+airlineID+"'";
			System.out.println(query);
			PreparedStatement pscheck = con.prepareStatement(query);
			ResultSet result = pscheck.executeQuery();
			
			while(result.next()) {
				String tmp = result.getString(1);
				if(tmp==null) tmp = "";
				
				String xml = "<flightNumber>"+tmp+"</flightNumber>";
				
				tmp = result.getString(2);
				if(tmp==null) tmp = "";
				
				xml = xml + "<airlineID>"+tmp+"</airlineID>";
				
				tmp = result.getString(3);
				if(tmp==null) tmp = "0";
				
				xml = xml + "<numberOfSeats>"+tmp+"</numberOfSeats>";
				
				tmp = result.getString(4);
				if(tmp==null) tmp = "";
				
				xml = xml + "<fareRestrict>"+tmp+"</fareRestrict>";
				
				tmp = result.getString(5);
				if(tmp==null) tmp = "0,1000";
				String tmp1 = tmp.substring(0, tmp.indexOf(","));
				String tmp2 = tmp.substring(tmp.indexOf(",")+1, tmp.length());
				xml = xml + "<lengthOfStayMin>"+tmp1+"</lengthOfStayMin>";
				xml = xml + "<lengthOfStayMax>"+tmp2+"</lengthOfStayMax>";
				xml = xml + "<lengthOfStay>"+tmp+"</lengthOfStay>";
				
				tmp = result.getString(6);
				if(tmp==null) tmp = "0";
				
				xml = xml + "<advancedPurchase>"+tmp+"</advancedPurchase>";
				
				tmp = result.getString(7);
				if(tmp==null) tmp = "0";
				
				xml = xml + "<fare>"+tmp+"</fare>";
				
				tmp = result.getString(8);
				if(tmp==null) tmp = "";
				
				xml = xml + "<departPort>"+tmp+"</departPort>";
				
				tmp = result.getString(9);
				if(tmp==null) tmp = "";
				
				xml = xml + "<departTime>"+tmp+"</departTime>";
				
				tmp = result.getString(10);
				if(tmp==null) tmp = "";
				
				xml = xml + "<arrivePort>"+tmp+"</arrivePort>";
				
				tmp = result.getString(11);
				if(tmp==null) tmp = "";
				
				xml = xml + "<arriveTime>"+tmp+"</arriveTime>";
				
				tmp = result.getString(12);
				if(tmp==null) tmp = "0";
				
				xml = xml + "<apdiscount>"+tmp+"</apdiscount>";
				ans = xml;
				
				
			}
			
			
		}catch(Exception e) {
			System.out.println("ERROR in getFlight : "+e);
		}
		
		try {
			db.closeConnection(con);
		}catch(Exception e) {}
		return ans;
	}

//GET AIRPORT INFORMATION BY AIRPORT ID
	public String getAirportNameCityCountry(String airportID){
		String ans = "";
		ApplicationDB db = new ApplicationDB();	
		Connection con = db.getConnection();
		
		try {
			String query = "";
			query = "select portName, portCity, portCountry from airports where airportID = '"+airportID+"';";
			System.out.println(query);
			PreparedStatement pscheck = con.prepareStatement(query);
			ResultSet result = pscheck.executeQuery();
			
			while(result.next()) {
				String tmp = result.getString(1);
				if(tmp==null) tmp = "";
				
				String xml = "<name>"+tmp+"</name>";
				
				tmp = result.getString(2);
				if(tmp==null) tmp = "";
				
				xml = xml + "<city>"+tmp+"</city>";

				
				tmp = result.getString(3);
				if(tmp==null) tmp = "";
				
				xml = xml + "<country>"+tmp+"</country>";
				
				ans = xml;
			}
			
			
		}catch(Exception e) {
			System.out.println("ERROR in getAirportInfo : "+e);
		}
		
		try {
			db.closeConnection(con);
		}catch(Exception e) {}
		return ans;
	}

//GET SEATS THAT ARE RESERVED FOR A PARTICULAR FLIGHT
	public Vector getReservedSeats(String flightNum, String airlineID, String departDateTime){
		Vector ans = new Vector();
		ApplicationDB db = new ApplicationDB();	
		Connection con = db.getConnection();
		
		try {
			String query = "";
			query = "select seatNum from reserve_flights "+
					"where flightNumber = '"+flightNum+"' and airlineID = '"+airlineID+"' and departDateTime = '"+departDateTime+"';";
			System.out.println(query);
			PreparedStatement pscheck = con.prepareStatement(query);
			ResultSet result = pscheck.executeQuery();
			
			while(result.next()) {
				String tmp = result.getString(1);
				if(tmp==null) tmp = "";
				
				ans.addElement(tmp);
				
			}
			
			
		}catch(Exception e) {
			System.out.println("ERROR in getReservedSeats : "+e);
		}
		
		try {
			db.closeConnection(con);
		}catch(Exception e) {}
		return ans;
	}

//CHECK IF SEAT IS TAKEN
	public boolean isTaken(Vector reservedSeats, String seat){
		
		for(int i = 0; i < reservedSeats.size(); i++){
			String reserved = (String)reservedSeats.get(i);
			if(seat.equals(reserved)){
				return true;
			}
		}
		
		return false;
	}

//APPLY ADVANCED PURCHASE DISCOUNT TO FARE
	public boolean applyAdvancedDiscount(String advancedPurchase, String flightDate) throws ParseException {
		System.out.println(advancedPurchase + " "+flightDate);
		int ap = Integer.parseInt(advancedPurchase);
		if(ap==0) {
			ap=10000;
		}
		Calendar calendar = Calendar.getInstance();
	    java.sql.Date today = new java.sql.Date(calendar.getTime().getTime());
	    calendar.add(Calendar.DATE, ap);
	    java.sql.Date today_and_ap = new java.sql.Date(calendar.getTime().getTime());
	    Date flight = new SimpleDateFormat("yyyy-MM-dd").parse(flightDate);
	    boolean ans = today_and_ap.before(flight);
		return ans;
	}

//CHECK IF ROUND TRIP BREAKS MINIMUM LENGTH REQUIREMENT
	public boolean breakMinLength(String startDate, String endDate, String minLength) throws ParseException {
		System.out.println(startDate+" "+ endDate+" "+ minLength);
		int min = Integer.parseInt(minLength);
		Calendar calendar = Calendar.getInstance();
		Date start = new SimpleDateFormat("yyyy-MM-dd").parse(startDate);
		Date end = new SimpleDateFormat("yyyy-MM-dd").parse(endDate);
		calendar.setTime(start);
		java.util.Date tmp = start;
		System.out.println(tmp.toString());
		System.out.println(start.toString());
		System.out.println(end.toString());
		if(tmp.toString().equals(end.toString())) {
			System.out.println("temp = end");
		}else {
			System.out.println("not equal");
		}
		int i = 1;
		for(i = 1; !tmp.toString().substring(0,10).equals(end.toString().substring(0,10)) && i < min; i++) {
			calendar.add(Calendar.DATE, 1);
			tmp = new java.util.Date(calendar.getTime().getTime());
			//System.out.println(i);
			System.out.println(i + " " + tmp.toString() + " "+end.toString());
		}
		
		//if(tmp.equals(end)) {
		//	return true;
		//}else {
		//	return false;
		//}
		if(i<min) {
			return true;
		}else {
			return false;
		}
	} 
	
//CHECK IF ROUND TRIP BREAKS MAXIMUM LENGTH REQUIREMENT
	public boolean breakMaxLength(String startDate, String endDate, String maxLength) throws ParseException {
		System.out.println(startDate +" "+ endDate + " " + maxLength);
		int max = Integer.parseInt(maxLength);
		Calendar calendar = Calendar.getInstance();
		Date start = new SimpleDateFormat("yyyy-MM-dd").parse(startDate);
		Date end = new SimpleDateFormat("yyyy-MM-dd").parse(endDate);
		calendar.setTime(start);
		java.util.Date tmp = start;
		System.out.println(tmp.toString());
		System.out.println(start.toString());
		System.out.println(end.toString());
		if(tmp.toString().equals(end.toString())) {
			System.out.println("temp = end");
		}else {
			System.out.println("not equal");
		}
		for(int i = 1; !tmp.toString().substring(0,10).equals(end.toString().substring(0,10))  && i < max; i++) {
			calendar.add(Calendar.DATE, 1);
			tmp = new java.util.Date(calendar.getTime().getTime());
			System.out.println(i + " " + tmp.toString() + " "+end.toString());
		}
		
		if(tmp.equals(end)) {
			return false;
		}else {
			return true;
		}
	} 

}

