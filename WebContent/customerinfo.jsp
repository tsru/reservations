<!-- Written by Tarun Sreenathan & Katarzyna Dobrzycka -->


<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="com.cs336.pkg.*"%>
<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*" %>
<jsp:useBean id="ctrl" scope="session" class="com.cs336.pkg.dbcalls"></jsp:useBean>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" name="viewport" content="text/html; charset=UTF-8, width=device-width, initial-scale=1">
<title>Customer Info</title>
<style>
html {
    display:table;
    margin:10px auto;
}
body {
    display:table-cell;
    vertical-align:middle;
}
h1,h2,h3,p,td{
	color:#EC407A;
	font-family:verdana;
}
</style>
</head>

<%
//CHECK IF DELETE CUSTOMER WAS SELECTED
		if(request.getParameter("delete")!=null && !request.getParameter("delete").equals("")){
			ctrl.deleteInfo(request.getParameter("delete"),true);
		}


String error  = "";

		if(request.getParameter("addemail")!=null && !request.getParameter("addemail").equals("")){
	//IF ADD CUSTOMER FORM WAS ADDED CREATE NEW ACCOUNT
			String e = request.getParameter("addemail");
			String f = request.getParameter("first");
			String l = request.getParameter("last");
			String a = request.getParameter("address");
			String c = request.getParameter("city");
			String st = request.getParameter("state");
			String z = request.getParameter("zip");
			String p = request.getParameter("phone");
			String cd = request.getParameter("createdate");
			String pw = request.getParameter("password");
			String cc = request.getParameter("creditcard");
			error = ctrl.addCustomer(f, l, a, c, st, z, p, e, cc, pw);
		}

//CHECK LOG IN CREDENTIALS
		boolean manage_custSignedin = false;
		String signedin = (String)session.getAttribute("signedin");
		if(signedin == null){
			signedin = "f";
		}
		if(signedin.equals("m") || signedin.equals("r")){
			manage_custSignedin = true;
		}
	
	if(manage_custSignedin){
//GET ALL CUSTOMERS' INFO
		Vector customers = ctrl.getInfo(false);

		
%>
<body>



<h1>Customer Info</h1>
<input type="button" value="Cancel" onClick="window.location.href='emplogin.jsp'">
<ul>
	<%
	//PRINT ALL CUSTOMERS' INFO
	for(int i = 0; i < customers.size(); i++){
		String xml = (String) customers.get(i);
		
		String accountnum = ctrl.getTagContent(xml, "account");
		String first = ctrl.getTagContent(xml, "first");
		String last = ctrl.getTagContent(xml, "last");
		String phone = ctrl.getTagContent(xml, "phone");
		String address = ctrl.getTagContent(xml, "address");
		String city = ctrl.getTagContent(xml, "city");
		String state = ctrl.getTagContent(xml, "state");
		String zip = ctrl.getTagContent(xml, "zip");
		String email = ctrl.getTagContent(xml, "email");
		String preferences = ctrl.getTagContent(xml, "preferences");
		String createDate = ctrl.getTagContent(xml, "createDate");
		String creditCard = ctrl.getTagContent(xml, "credit");
		
		out.println("<li>"+first+" "+last+": account number - "+accountnum+"<br>"+
				"phone - "+phone+"<br>"+
				"email - "+email+"<br>"+
				"address - "+address+" "+city+" "+state+" "+zip+"<br>"+
				"preferences - "+preferences+"<br>"+
				"create date - "+createDate+"<br>"+
				"credit card - "+creditCard);
		
		out.println("<input type=\"button\" value=\"delete\" onClick=\"window.location.href='customerinfo.jsp?delete="+accountnum+"'\">"+
				"<input type=\"button\" value=\"edit\" onClick=\"window.location.href='editInfo.jsp?accountnum="+accountnum+"'\">"+
				"</li>");
	}
	
	%>
</ul>
<!-- FORM FOR ADDING NEW CUSTOMER -->
<h3>Add new customer: </h3>
<form>
<input type="text" name="addemail" placeholder="Email">
<input type="text" name="first" placeholder="First name">
<input type="text" name="last" placeholder="Last name">
<input type="text" name="address" placeholder="Address">
<input type="text" name="city" placeholder="City">
<input type="text" name="state" placeholder="State">
<input type="text" name="zip" placeholder="Zip">
<input type="text" name="phone" placeholder="Phone">
<input type="text" name="preferences" placeholder="Preferences">
<input type="text" name="creditcard" placeholder="Credit card">
<input type="submit" value="Add customer">
<%=error%>
</form>





</body>

<% }else{ 
		
		out.println("You are not signed in as a manager or customer representative");
		out.println("<a href=\"emplogin.jsp\">Go to login</a>");
			
	}

%>
</html>