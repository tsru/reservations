<!-- Written by Tarun Sreenathan & Katarzyna Dobrzycka -->


<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="com.cs336.pkg.*"%>
<%@ page import="java.io.*,java.util.*,java.sql.*,java.util.Date,java.text.SimpleDateFormat"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*" %>
<jsp:useBean id="ctrl" scope="session" class="com.cs336.pkg.dbcalls"></jsp:useBean>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" name="viewport" content="text/html; charset=UTF-8, width=device-width, initial-scale=1">
<title>Make Reservation</title>
<style>
html {
    display:table;
    margin:10px auto;
}
body {
    display:table-cell;
    vertical-align:middle;
}
h1,h2,h3,p,td{
	color:#EC407A;
	font-family:verdana;
}
</style>
</head>

<%
String reserveNum = "";

		boolean custsignedin = false;
		String signedin = (String)session.getAttribute("signedin");
		String account = (String)session.getAttribute("account");
		String email = (String)session.getAttribute("email");
		if(signedin == null ||
				signedin.equals("m")||
				signedin.equals("r")||
				signedin.equals("f")){
		}else{
			custsignedin = true;
		}

		String log_out = request.getParameter("off");
		if(log_out != null && !log_out.isEmpty()){
			signedin = "f";
			custsignedin = false;
			session.setAttribute("signedin", signedin);
			session.removeAttribute("email");
		}
		
		Vector airports = ctrl.getAirports();
		
		
		
		//READ RESERVATION REQUEST
		String t = request.getParameter("type");
		int type = Integer.parseInt(t);
			//1 = one way. 2 = round trip. 3 = multicity
		//Vector results = new Vector();
		String infoxml = "";
			
		
		
		//type 1 variables
		String depart = "";
		String flightNum = "";
		String airline = "";
		String dateTime = "";
		String date = "";
		String departPort = "";
		String arrivePort = "";
		String seatsLeft = "";
		String departTime = "";
		String arriveTime = "";
		String fare ="";
		String advpur = "";
		String advper = "";
		boolean advancedPurchase = false;
		
		//type2 variables
		String returntrip = "";
		String flightNum1 = "";
		String flightNum2 = "";
		String airline1 = "";
		String airline2 = "";
		String dateTime1 = "";
		String dateTime2 = "";
		String date1 = "";
		String date2 = "";
		String departTime1 = "";
		String departTime2 = "";
		String arriveTime1 = "";
		String arriveTime2 = "";
		String fare1 = "";
		String fare2 = "";
		String departPort1 = "";
		String departPort2 = "";
		String arrivePort1 = "";
		String arrivePort2 = ""; 
		String advpur1 = "";
		String advpur2 = "";
		String advper1 = "";
		String advper2 = "";
		boolean advancedPurchase1 = false;
		boolean advancedPurchase2 = false;
		boolean breakMinLength = false;
		boolean breakMaxLength = false;
		
		//type3 variables
		String depart1 = "";
		String depart2 = "";
		String depart3 = "";
		String depart4 = "";
		String flightNum3 = "";
		String flightNum4 = "";
		String airline3 = "";
		String airline4 = "";
		String dateTime3 = "";
		String dateTime4 = "";
		String date3 = "";
		String date4 = "";
		String departTime3 = "";
		String departTime4 = "";
		String arriveTime3 = "";
		String arriveTime4 = "";
		String fare3 = "";
		String fare4 = "";
		String departPort3 = "";
		String departPort4 = "";
		String arrivePort3 = "";
		String arrivePort4 = "";
		String advpur3 = "";
		String advpur4 = "";
		String advper3 = "";
		String advper4 = "";
		boolean advancedPurchase3 = false;
		boolean advancedPurchase4 = false;
		
		
		//if(type == 1 && (request.getParameter("reserve") == null || request.getParameter("reserve").equals(""))){
		if(type == 1){
			flightNum = request.getParameter("flightNum");
			airline = request.getParameter("airlineID");
			dateTime = request.getParameter("dateTime");
			date = request.getParameter("date");
			infoxml = ctrl.getFlightInfo(flightNum, airline);
			departPort = ctrl.getTagContent(infoxml, "departPort");
			arrivePort = ctrl.getTagContent(infoxml, "arrivePort");
			departTime = ctrl.getTagContent(infoxml, "departTime");
			arriveTime = ctrl.getTagContent(infoxml, "arriveTime");
			advpur = ctrl.getTagContent(infoxml, "advancedPurchase");
			advper = ctrl.getTagContent(infoxml, "apdiscount");			
			fare = ctrl.getTagContent(infoxml, "fare");
			advancedPurchase = ctrl.applyAdvancedDiscount(advpur, date);
			if(advancedPurchase){
				float per = Float.parseFloat(advper);
				float f = Float.parseFloat(fare);
				f = f - (f*per);
				fare = "" + f;
			}
		//}else if(type==2 && (request.getParameter("reserve") == null || request.getParameter("reserve").equals(""))){
		}else if(type==2){
			depart = request.getParameter("depart");
			System.out.println("DEPART: "+depart);
			int a = depart.indexOf(",");
			flightNum1 = depart.substring(0, a);
			depart = depart.substring(a+1, depart.length());
			a = depart.indexOf(",");
			airline1 = depart.substring(0,a);
			dateTime1 = depart.substring(a+1, depart.lastIndexOf(","));
			depart = depart.substring(depart.lastIndexOf(",")+1, depart.length());
			date1 = depart;
			infoxml = ctrl.getFlightInfo(flightNum1, airline1);
			fare1 = ctrl.getTagContent(infoxml, "fare");
			departPort1 = ctrl.getTagContent(infoxml, "departPort");
			arrivePort1 = ctrl.getTagContent(infoxml, "arrivePort");
			advpur1 = ctrl.getTagContent(infoxml, "advancedPurchase");
			advper1 = ctrl.getTagContent(infoxml, "apdiscount");			
			advancedPurchase1 = ctrl.applyAdvancedDiscount(advpur1, date1);
			depart = request.getParameter("depart");
			String lengthOfStayMin = ctrl.getTagContent(infoxml, "lengthOfStayMin");
			String lengthOfStayMax = ctrl.getTagContent(infoxml, "lengthOfStayMax");
			
			returntrip = request.getParameter("return");
			System.out.println(returntrip);
			a = returntrip.indexOf(",");
			flightNum2 = returntrip.substring(0, a);
			returntrip = returntrip.substring(a+1, returntrip.length());
			a = returntrip.indexOf(",");
			airline2 = returntrip.substring(0,a);
			dateTime2 = returntrip.substring(a+1, returntrip.lastIndexOf(","));
			returntrip = returntrip.substring(returntrip.lastIndexOf(",")+1, returntrip.length());
			date2 = returntrip;
			infoxml = ctrl.getFlightInfo(flightNum2, airline2);
			fare2 = ctrl.getTagContent(infoxml, "fare");
			departPort2 = ctrl.getTagContent(infoxml, "departPort");
			arrivePort2 = ctrl.getTagContent(infoxml, "arrivePort");
			advpur2 = ctrl.getTagContent(infoxml, "advancedPurchase");
			advper2 = ctrl.getTagContent(infoxml, "apdiscount");	
			advancedPurchase2 = ctrl.applyAdvancedDiscount(advpur2, date2);
			returntrip = request.getParameter("return");
			breakMinLength = ctrl.breakMinLength(date1, date2, lengthOfStayMin);
			breakMaxLength = ctrl.breakMaxLength(date1, date2, lengthOfStayMax);
			
			
			if(advancedPurchase1){
				float per = Float.parseFloat(advper1);
				float f = Float.parseFloat(fare1);
				f = f - (f*per);
				fare1 = "" + f;
			}
			if(advancedPurchase2){
				float per = Float.parseFloat(advper2);
				float f = Float.parseFloat(fare2);
				f = f - (f*per);
				fare2 = "" + f;
			}
		//}else if(type==3 && (request.getParameter("reserve") == null || request.getParameter("reserve").equals(""))){
		}else if(type==3){
			depart1 = request.getParameter("depart1");
				System.out.println(depart1);
			int a = depart1.indexOf(",");
			flightNum1 = depart1.substring(0, a);
			depart1 = depart1.substring(a+1, depart1.length());
			a = depart1.indexOf(",");
			airline1 = depart1.substring(0,a);
			dateTime1 = depart1.substring(a+1, depart1.lastIndexOf(","));
			depart1 = depart1.substring(depart1.lastIndexOf(",")+1, depart1.length());
			date1 = depart1;			
			infoxml = ctrl.getFlightInfo(flightNum1, airline1);
			fare1 = ctrl.getTagContent(infoxml, "fare");
			departPort1 = ctrl.getTagContent(infoxml, "departPort");
			arrivePort1 = ctrl.getTagContent(infoxml, "arrivePort");
			advpur1 = ctrl.getTagContent(infoxml, "advancedPurchase");
			advper1 = ctrl.getTagContent(infoxml, "apdiscount");			
			advancedPurchase1 = ctrl.applyAdvancedDiscount(advpur1, date1);
			depart1 = request.getParameter("depart1");
			
			depart2 = request.getParameter("depart2");
			System.out.println(depart2);
			
			a = depart2.indexOf(",");
			flightNum2 = depart2.substring(0, a);
			depart2 = depart2.substring(a+1, depart2.length());
			a = depart2.indexOf(",");
			airline2 = depart2.substring(0,a);
			dateTime2 = depart2.substring(a+1, depart2.lastIndexOf(","));
			depart2 = depart2.substring(depart2.lastIndexOf(",")+1, depart2.length());
			date2 = depart2;
			infoxml = ctrl.getFlightInfo(flightNum2, airline2);
			fare2 = ctrl.getTagContent(infoxml, "fare");
			departPort2 = ctrl.getTagContent(infoxml, "departPort");
			arrivePort2 = ctrl.getTagContent(infoxml, "arrivePort");
			advpur2 = ctrl.getTagContent(infoxml, "advancedPurchase");
			advper2 = ctrl.getTagContent(infoxml, "apdiscount");			
			advancedPurchase2 = ctrl.applyAdvancedDiscount(advpur2, date2);
			depart2 = request.getParameter("depart2");
			
			depart3 = request.getParameter("depart3");
			System.out.println(depart3);
			
			a = depart3.indexOf(",");
			flightNum3 = depart3.substring(0, a);
			depart3 = depart3.substring(a+1, depart3.length());
			a = depart3.indexOf(",");
			airline3 = depart3.substring(0,a);
			dateTime3 = depart3.substring(a+1, depart3.lastIndexOf(","));
			depart3 = depart3.substring(depart3.lastIndexOf(",")+1, depart3.length());
			date3 = depart3;
			infoxml = ctrl.getFlightInfo(flightNum3, airline3);
			fare3 = ctrl.getTagContent(infoxml, "fare");
			departPort3 = ctrl.getTagContent(infoxml, "departPort");
			arrivePort3 = ctrl.getTagContent(infoxml, "arrivePort");
			advpur3 = ctrl.getTagContent(infoxml, "advancedPurchase");
			advper3 = ctrl.getTagContent(infoxml, "apdiscount");			
			advancedPurchase3 = ctrl.applyAdvancedDiscount(advpur3, date3);
			depart3 = request.getParameter("depart3");
			
			depart4 = request.getParameter("depart4");
			System.out.println(depart4);
			
			a = depart4.indexOf(",");
			flightNum4 = depart4.substring(0, a);
			depart4 = depart4.substring(a+1, depart4.length());
			a = depart4.indexOf(",");
			airline4 = depart4.substring(0,a);
			dateTime4 = depart4.substring(a+1, depart4.lastIndexOf(","));
			depart4 = depart4.substring(depart4.lastIndexOf(",")+1, depart4.length());
			date4 = depart4;
			infoxml = ctrl.getFlightInfo(flightNum4, airline4);
			fare4 = ctrl.getTagContent(infoxml, "fare");
			departPort4 = ctrl.getTagContent(infoxml, "departPort");
			arrivePort4 = ctrl.getTagContent(infoxml, "arrivePort");
			advpur4 = ctrl.getTagContent(infoxml, "advancedPurchase");
			advper4 = ctrl.getTagContent(infoxml, "apdiscount");			
			advancedPurchase4 = ctrl.applyAdvancedDiscount(advpur4, date4);
			depart4 = request.getParameter("depart4");
			
			if(advancedPurchase1){
				float per = Float.parseFloat(advper1);
				float f = Float.parseFloat(fare1);
				f = f - (f*per);
				fare1 = "" + f;
			}
			if(advancedPurchase2){
				float per = Float.parseFloat(advper2);
				float f = Float.parseFloat(fare2);
				f = f - (f*per);
				fare2 = "" + f;
			}
			if(advancedPurchase3){
				float per = Float.parseFloat(advper3);
				float f = Float.parseFloat(fare3);
				f = f - (f*per);
				fare3 = "" + f;
			}
			if(advancedPurchase4){
				float per = Float.parseFloat(advper4);
				float f = Float.parseFloat(fare4);
				f = f - (f*per);
				fare4 = "" + f;
			}
		}
		
		
		
		
		
		
		String reject = "";
		if(custsignedin){
		//RESERVATION HAS BEEN SUBMITTED:
		String requestSent = request.getParameter("reserve");
		
		if(requestSent!=null && !requestSent.equals("")){
			if(type==1){
				String meal = request.getParameter("meal");
				String seatClass = request.getParameter("class");
				String seatNum = request.getParameter("seatNum");
				
				reserveNum = ctrl.addReservationSelf(flightNum, airline, dateTime, meal, seatClass, seatNum, fare, account);
				
			}else if(type==2){
				String meal1 = request.getParameter("meal1");
				String seatClass1 = request.getParameter("class1");
				String seatNum1 = request.getParameter("seat1");
				String meal2 = request.getParameter("meal2");
				String seatClass2 = request.getParameter("class2");
				String seatNum2 = request.getParameter("seat2");
				
				reserveNum = ctrl.addReservationSelfRoundtrip(flightNum1, flightNum2, airline1, airline2, dateTime1, dateTime2, meal1, meal2, seatClass1, seatClass2, seatNum1, seatNum2, fare1, fare2, account);
				
			}else if(type==3){
				String meal1 = request.getParameter("meal1");
				String seatClass1 = request.getParameter("class1");
				String seatNum1 = request.getParameter("seat1");
				String meal2 = request.getParameter("meal2");
				String seatClass2 = request.getParameter("class2");
				String seatNum2 = request.getParameter("seat2");
				String meal3 = request.getParameter("meal3");
				String seatClass3 = request.getParameter("class3");
				String seatNum3 = request.getParameter("seat3");
				String meal4 = request.getParameter("meal4");
				String seatClass4 = request.getParameter("class4");
				String seatNum4 = request.getParameter("seat4");
				
				reserveNum = ctrl.addReservationSelfMulti(flightNum1, flightNum2, flightNum3, flightNum4, airline1, airline2, airline3, airline4, dateTime1, dateTime2, dateTime3, dateTime4, meal1, meal2, meal3, meal4, seatClass1, seatClass2, seatClass3, seatClass4, seatNum1, seatNum2, seatNum3, seatNum4, fare1, fare2, fare3, fare4, account);
			}
		}
		
		}else{
			reject = "You must be signed in to create a reservation.  Click below to log in or create an account"+
					"<br><input type=\"button\" value=\"Sign up\" onClick=\"window.location.href='signup.jsp'\">"+
					"<br><input type=\"button\" value=\"Log In\" onClick=\"window.location.href='custlogin.jsp'\">";
		}
	%>

<body>

<%
if(custsignedin){
	%><form>
	<tr>
	<td><input type='submit' value='Log Out' name='off'></td>
	</tr>
	</form>
	<input type="button" value="My Account" onClick="window.location.href='custlogin.jsp'">
	<%
	
}else{
	%>
	<!-- <input type="button" value="Log In" onClick="window.location.href='custlogin.jsp'">-->
	
	
	<%	
	
	
}
	
		
%>
<input type='button' value='Home' onClick="window.location.href='index.jsp'">
	
<h1>Create reservation:</h1>


<% if(type == 1){ %>
<form>
<ul>
<input type="hidden" name="reserve" value="true">
<input type="hidden" name="type" value="<%=type%>">
<li>Flight Number:  <%=flightNum %></li>
<input type="hidden" name="flightNum" value="<%=flightNum%>">
<li>Airline: <%=airline %></li>
<input type="hidden" name="airlineID" value="<%=airline%>">
<li>Depart from: <%=departPort %></li>
<input type="hidden" name="departPort" value="<%=departPort%>">
<li>Destination: <%=arrivePort %></li>
<input type="hidden" name="arrivePort" value="<%=arrivePort%>">
<li>Date and time of departure: <%=dateTime%></li>
<input type="hidden" name="dateTime" value="<%=dateTime%>">
<input type="hidden" name="date" value="<%=date %>">
<li>Fare: <%=fare %></li>
<li>Booking fee: <%
	double f = Double.parseDouble(fare);
	f = f * (.1);
	out.println(f);
%></li>
<%
	if(advancedPurchase){
		%>
		<p>Advanced purchase discount has been applied!</p>
		<%
	}
%>
<input type="hidden" name="fare" value="<%=fare%>">
<li>
<div>
<label>Meal</label>
	<select name="meal">
		<option value="basic">Basic</option>
		<option value="vegetarian">Vegetarian</option>
		<option value="deluxe">Deluxe</option>
	</select>
</div>
</li>
<li>
<div>
<label>Class</label>
	<select name="class">
		<option value="first">First</option>
		<option value="business">Business</option>
		<option value="economy">Economy</option>
	</select>
</div>
</li>

<li>
<div>
<label>Seat Number (A,F are window seats, C,D are aisle seats) </label>
<select name="seatNum">
<%
	String info = ctrl.getFlightInfo(flightNum, airline);
	String seats = ctrl.getTagContent(info, "numberOfSeats");
	int seatsmax = Integer.parseInt(seats);
	Vector takenSeats = ctrl.getReservedSeats(flightNum, airline, dateTime);
	int j = 1;
	for(int i = 1; i < seatsmax; i++){
		String seat = ""+j;
		if(i%6 == 1){
			seat = seat+"A";
		}else if(i%6 == 2){
			seat = seat + "B";
		}else if(i%6 == 3){
			seat = seat + "C";
		}else if(i%6 == 4){
			seat = seat + "D";
		}else if(i%6 == 5){
			seat = seat + "E";
		}else if(i%6 ==0){
			seat = seat + "F";
			j++;
		}
		boolean taken = ctrl.isTaken(takenSeats, seat);
		if(!taken){
			out.println("<option value=\""+seat+"\">"+seat+"</option>");
		}
	}


%>



	</select>
</div>
</li>

	

</ul>


<% if(custsignedin){ %>
<input type="submit" value="Reserve">
<%
}else{
	out.println(reject);
}
%>
</form>

<% }else if(type==2){ %>

<form>
<ul>
<input type="hidden" name="reserve" value="true">
<input type="hidden" name="type" value="<%=type%>">
<input type="hidden" name="depart" value="<%=depart %>">
<input type="hidden" name="return" value="<%=returntrip %>">
<li>Flight Number:  <%=flightNum1 %></li>
<input type="hidden" name="flightNum1" value="<%=flightNum1%>">
<li>Airline: <%=airline1 %></li>
<input type="hidden" name="airlineID1" value="<%=airline1%>">
<li>Depart from: <%=departPort1 %></li>
<input type="hidden" name="departPort1" value="<%=departPort1%>">
<li>Destination: <%=arrivePort1 %></li>
<input type="hidden" name="arrivePort1" value="<%=arrivePort1%>">
<li>Date and time of departure: <%=dateTime1%></li>
<input type="hidden" name="dateTime1" value="<%=dateTime1%>">
<li>Fare: <%=fare1 %></li>
<li>Booking fee: <%
	double f = Double.parseDouble(fare1);
	f = f * (.1);
	out.println(f);
%></li>
<%
	if(advancedPurchase1){
		%>
		<p>Advanced purchase discount has been applied!</p>
		<%
	}
%>
<input type="hidden" name="fare1" value="<%=fare1%>">
<li>
<div>
<label>Meal</label>
	<select name="meal1">
		<option value="basic">Basic</option>
		<option value="vegetarian">Vegetarian</option>
		<option value="deluxe">Deluxe</option>
	</select>
</div>
</li>
<li>
<div>
<label>Class</label>
	<select name="class1">
		<option value="first">First</option>
		<option value="business">Business</option>
		<option value="economy">Economy</option>
	</select>
</div>
</li>
<li>
<div>
<label>Seat Number (A,F are window seats, C,D are aisle seats) </label>
<select name="seat1">
<%
	String info = ctrl.getFlightInfo(flightNum1, airline1);
	String seats = ctrl.getTagContent(info, "numberOfSeats");
	int seatsmax = Integer.parseInt(seats);
	Vector takenSeats = ctrl.getReservedSeats(flightNum1, airline1, dateTime1);
	int j = 1;
	for(int i = 1; i < seatsmax; i++){
		String seat = ""+j;
		if(i%6 == 1){
			seat = seat+"A";
		}else if(i%6 == 2){
			seat = seat + "B";
		}else if(i%6 == 3){
			seat = seat + "C";
		}else if(i%6 == 4){
			seat = seat + "D";
		}else if(i%6 == 5){
			seat = seat + "E";
		}else if(i%6 ==0){
			seat = seat + "F";
			j++;
		}
		boolean taken = ctrl.isTaken(takenSeats, seat);
		if(!taken){
			out.println("<option value=\""+seat+"\">"+seat+"</option>");
		}
	}


%>



	</select>
</div>
</li>
<br><br>
<li>Flight Number:  <%=flightNum2 %></li>
<input type="hidden" name="flightNum2" value="<%=flightNum2%>">
<li>Airline: <%=airline2 %></li>
<input type="hidden" name="airline2" value="<%=airline2%>">
<li>Depart from: <%=departPort2 %></li>
<input type="hidden" name="departPort2" value="<%=departPort2%>">
<li>Destination: <%=arrivePort2 %></li>
<input type="hidden" name="arrivePort2" value="<%=arrivePort2%>">
<li>Date and time of departure: <%=dateTime2%></li>
<input type="hidden" name="dateTime2" value="<%=dateTime2%>">
<li>Fare: <%=fare2 %></li>
<li>Booking fee: <%
	f = Double.parseDouble(fare2);
	f = f * (.1);
	out.println(f);
%></li>
<%
	if(advancedPurchase2){
		%>
		<p>Advanced purchase discount has been applied!</p>
		<%
	}
%>
<input type="hidden" name="fare2" value="<%=fare2%>">
<li>
<div>
<label>Meal</label>
	<select name="meal2">
		<option value="basic">Basic</option>
		<option value="vegetarian">Vegetarian</option>
		<option value="deluxe">Deluxe</option>
	</select>
</div>
</li>
<li>
<div>
<label>Class</label>
	<select name="class2">
		<option value="first">First</option>
		<option value="business">Business</option>
		<option value="economy">Economy</option>
	</select>
</div>
</li>
<li>
<div>
<label>Seat Number (A,F are window seats, C,D are aisle seats) </label>
<select name="seat2">
<%
	info = ctrl.getFlightInfo(flightNum2, airline2);
	seats = ctrl.getTagContent(info, "numberOfSeats");
	seatsmax = Integer.parseInt(seats);
	takenSeats = ctrl.getReservedSeats(flightNum2, airline2, dateTime2);
	j = 1;
	for(int i = 1; i < seatsmax; i++){
		String seat = ""+j;
		if(i%6 == 1){
			seat = seat+"A";
		}else if(i%6 == 2){
			seat = seat + "B";
		}else if(i%6 == 3){
			seat = seat + "C";
		}else if(i%6 == 4){
			seat = seat + "D";
		}else if(i%6 == 5){
			seat = seat + "E";
		}else if(i%6 ==0){
			seat = seat + "F";
			j++;
		}
		boolean taken = ctrl.isTaken(takenSeats, seat);
		if(!taken){
			out.println("<option value=\""+seat+"\">"+seat+"</option>");
		}
	}


%>



	</select>
</div>
</li>
</ul>


<% if(custsignedin){ 

	if(breakMinLength){
		out.println("<h3>Your request is below the minimum trip length for your departure flight.</h3>");
	}else if(breakMaxLength){
		out.println("<h3>Your request is above the maximum trip length for your departure flight.</h3>");
	}else{
		%>
		<input type="submit" value="Reserve">
		<%
	}

}else{
	out.println(reject);
}
%>
</form>



<% }else if(type==3){ %>


<form>
<ul>
<input type="hidden" name="reserve" value="true">
<input type="hidden" name="type" value="<%=type%>">
<input type="hidden" name="depart1" value="<%=depart1 %>">
<input type="hidden" name="depart2" value="<%=depart2 %>">
<input type="hidden" name="depart3" value="<%=depart3 %>">
<input type="hidden" name="depart4" value="<%=depart4 %>">

<li>Flight Number:  <%=flightNum1 %></li>
<input type="hidden" name="flightNum1" value="<%=flightNum1%>">
<li>Airline: <%=airline1 %></li>
<input type="hidden" name="airlineID1" value="<%=airline1%>">
<li>Depart from: <%=departPort1 %></li>
<input type="hidden" name="departPort1" value="<%=departPort1%>">
<li>Destination: <%=arrivePort1 %></li>
<input type="hidden" name="arrivePort1" value="<%=arrivePort1%>">
<li>Date and time of departure: <%=dateTime1%></li>
<input type="hidden" name="dateTime1" value="<%=dateTime1%>">
<li>Fare: <%=fare1 %></li>
<li>Booking fee: <%
	double f = Double.parseDouble(fare1);
	f = f * (.1);
	out.println(f);
%></li>
<%
	if(advancedPurchase1){
		%>
		<p>Advanced purchase discount has been applied!</p>
		<%
	}
%>
<input type="hidden" name="fare1" value="<%=fare1%>">
<li>
<div>
<label>Meal</label>
	<select name="meal1">
		<option value="basic">Basic</option>
		<option value="vegetarian">Vegetarian</option>
		<option value="deluxe">Deluxe</option>
	</select>
</div>
</li>
<li>
<div>
<label>Class</label>
	<select name="class1">
		<option value="first">First</option>
		<option value="business">Business</option>
		<option value="economy">Economy</option>
	</select>
</div>
</li>
<li>
<div>
<label>Seat Number (A,F are window seats, C,D are aisle seats) </label>
<select name="seat1">
<%
	String info = ctrl.getFlightInfo(flightNum1, airline1);
	String seats = ctrl.getTagContent(info, "numberOfSeats");
	int seatsmax = Integer.parseInt(seats);
	Vector takenSeats = ctrl.getReservedSeats(flightNum1, airline1, dateTime1);
	int j = 1;
	for(int i = 1; i < seatsmax; i++){
		String seat = ""+j;
		if(i%6 == 1){
			seat = seat+"A";
		}else if(i%6 == 2){
			seat = seat + "B";
		}else if(i%6 == 3){
			seat = seat + "C";
		}else if(i%6 == 4){
			seat = seat + "D";
		}else if(i%6 == 5){
			seat = seat + "E";
		}else if(i%6 ==0){
			seat = seat + "F";
			j++;
		}
		boolean taken = ctrl.isTaken(takenSeats, seat);
		if(!taken){
			out.println("<option value=\""+seat+"\">"+seat+"</option>");
		}
	}


%>



	</select>
</div>
</li>
<br><br>
<li>Flight Number:  <%=flightNum2 %></li>
<input type="hidden" name="flightNum2" value="<%=flightNum2%>">
<li>Airline: <%=airline2 %></li>
<input type="hidden" name="airline2" value="<%=airline2%>">
<li>Depart from: <%=departPort2 %></li>
<input type="hidden" name="departPort2" value="<%=departPort2%>">
<li>Destination: <%=arrivePort2 %></li>
<input type="hidden" name="arrivePort2" value="<%=arrivePort2%>">
<li>Date and time of departure: <%=dateTime2%></li>
<input type="hidden" name="dateTime2" value="<%=dateTime2%>">
<li>Fare: <%=fare2 %></li>
<li>Booking fee: <%
	f = Double.parseDouble(fare2);
	f = f * (.1);
	out.println(f);
%></li>
<%
	if(advancedPurchase2){
		%>
		<p>Advanced purchase discount has been applied!</p>
<%
	}
%>
<input type="hidden" name="fare2" value="<%=fare2%>">
<li>
<div>
<label>Meal</label>
	<select name="meal2">
		<option value="basic">Basic</option>
		<option value="vegetarian">Vegetarian</option>
		<option value="deluxe">Deluxe</option>
	</select>
</div>
</li>
<li>
<div>
<label>Class</label>
	<select name="class2">
		<option value="first">First</option>
		<option value="business">Business</option>
		<option value="economy">Economy</option>
	</select>
</div>
</li>
<li>
<div>
<label>Seat Number (A,F are window seats, C,D are aisle seats) </label>
<select name="seat2">
<%
	info = ctrl.getFlightInfo(flightNum2, airline2);
	seats = ctrl.getTagContent(info, "numberOfSeats");
	seatsmax = Integer.parseInt(seats);
	takenSeats = ctrl.getReservedSeats(flightNum2, airline2, dateTime2);
	j = 1;
	for(int i = 1; i < seatsmax; i++){
		String seat = ""+j;
		if(i%6 == 1){
			seat = seat+"A";
		}else if(i%6 == 2){
			seat = seat + "B";
		}else if(i%6 == 3){
			seat = seat + "C";
		}else if(i%6 == 4){
			seat = seat + "D";
		}else if(i%6 == 5){
			seat = seat + "E";
		}else if(i%6 ==0){
			seat = seat + "F";
			j++;
		}
		boolean taken = ctrl.isTaken(takenSeats, seat);
		if(!taken){
			out.println("<option value=\""+seat+"\">"+seat+"</option>");
		}
	}


%>



	</select>
</div>
</li>
<br><br>

<li>Flight Number:  <%=flightNum3 %></li>
<input type="hidden" name="flightNum3" value="<%=flightNum3%>">
<li>Airline: <%=airline3 %></li>
<input type="hidden" name="airline3" value="<%=airline3%>">
<li>Depart from: <%=departPort3 %></li>
<input type="hidden" name="departPort3" value="<%=departPort3%>">
<li>Destination: <%=arrivePort3 %></li>
<input type="hidden" name="arrivePort3" value="<%=arrivePort3%>">
<li>Date and time of departure: <%=dateTime3%></li>
<input type="hidden" name="dateTime3" value="<%=dateTime3%>">
<li>Fare: <%=fare3 %></li>
<li>Booking fee: <%
	f = Double.parseDouble(fare3);
	f = f * (.1);
	out.println(f);
%></li>
<%
	if(advancedPurchase3){
		%>
		<p>Advanced purchase discount has been applied!</p>
		<%
	}
%>
<input type="hidden" name="fare3" value="<%=fare3%>">
<li>
<div>
<label>Meal</label>
	<select name="meal3">
		<option value="basic">Basic</option>
		<option value="vegetarian">Vegetarian</option>
		<option value="deluxe">Deluxe</option>
	</select>
</div>
</li>
<li>
<div>
<label>Class</label>
	<select name="class3">
		<option value="first">First</option>
		<option value="business">Business</option>
		<option value="economy">Economy</option>
	</select>
</div>
</li>
<li>
<div>
<label>Seat Number (A,F are window seats, C,D are aisle seats) </label>
<select name="seat3">
<%
	info = ctrl.getFlightInfo(flightNum3, airline3);
	seats = ctrl.getTagContent(info, "numberOfSeats");
	seatsmax = Integer.parseInt(seats);
	takenSeats = ctrl.getReservedSeats(flightNum3, airline3, dateTime3);
	j = 1;
	for(int i = 1; i < seatsmax; i++){
		String seat = ""+j;
		if(i%6 == 1){
			seat = seat+"A";
		}else if(i%6 == 2){
			seat = seat + "B";
		}else if(i%6 == 3){
			seat = seat + "C";
		}else if(i%6 == 4){
			seat = seat + "D";
		}else if(i%6 == 5){
			seat = seat + "E";
		}else if(i%6 ==0){
			seat = seat + "F";
			j++;
		}
		boolean taken = ctrl.isTaken(takenSeats, seat);
		if(!taken){
			out.println("<option value=\""+seat+"\">"+seat+"</option>");
		}
	}


%>



	</select>
</div>
</li>
<br><br>


<li>Flight Number:  <%=flightNum4 %></li>
<input type="hidden" name="flightNum4" value="<%=flightNum4%>">
<li>Airline: <%=airline4 %></li>
<input type="hidden" name="airline4" value="<%=airline4%>">
<li>Depart from: <%=departPort4 %></li>
<input type="hidden" name="departPort4" value="<%=departPort4%>">
<li>Destination: <%=arrivePort4 %></li>
<input type="hidden" name="arrivePort4" value="<%=arrivePort4%>">
<li>Date and time of departure: <%=dateTime4%></li>
<input type="hidden" name="dateTime4" value="<%=dateTime4%>">
<li>Fare: <%=fare4 %></li>
<li>Booking fee: <%
	f = Double.parseDouble(fare4);
	f = f * (.1);
	out.println(f);
%></li>
<%
	if(advancedPurchase4){
		%>
		<p>Advanced purchase discount has been applied!</p>
		<%
	}
%>
<input type="hidden" name="fare4" value="<%=fare4%>">
<li>
<div>
<label>Meal</label>
	<select name="meal4">
		<option value="basic">Basic</option>
		<option value="vegetarian">Vegetarian</option>
		<option value="deluxe">Deluxe</option>
	</select>
</div>
</li>
<li>
<div>
<label>Class</label>
	<select name="class4">
		<option value="first">First</option>
		<option value="business">Business</option>
		<option value="economy">Economy</option>
	</select>
</div>
</li>
<li>
<div>
<label>Seat Number (A,F are window seats, C,D are aisle seats) </label>
<select name="seat4">


	<%
	
	info = ctrl.getFlightInfo(flightNum4, airline4);
	seats = ctrl.getTagContent(info, "numberOfSeats");
	seatsmax = Integer.parseInt(seats);
	takenSeats = ctrl.getReservedSeats(flightNum4, airline4, dateTime4);
	j = 1;
	for(int i = 1; i < seatsmax; i++){
		String seat = ""+j;
		if(i%6 == 1){
			seat = seat+"A";
		}else if(i%6 == 2){
			seat = seat + "B";
		}else if(i%6 == 3){
			seat = seat + "C";
		}else if(i%6 == 4){
			seat = seat + "D";
		}else if(i%6 == 5){
			seat = seat + "E";
		}else if(i%6 ==0){
			seat = seat + "F";
			j++;
		}
		boolean taken = ctrl.isTaken(takenSeats, seat);
		if(!taken){
			out.println("<option value=\""+seat+"\">"+seat+"</option>");
		}
	}


%>



	</select>
</div>
</li>



</ul>



	<% if(custsignedin){ %>
<input type="submit" value="Reserve">
<%
}else{
	out.println(reject);
}
%>
</form>




<% } 


if(reserveNum != null && !reserveNum.equals("")){
	
	out.println("<h4>Reservation successful!  Go to your reservation portfolio to view your reservation by clicking below: </h4>");
	out.println("<input type=\"button\" value=\"My Reservations\" onClick=\"window.location.href='myreservations.jsp'\">");
		
}
%>
</body>
</html>