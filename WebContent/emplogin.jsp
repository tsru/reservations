<!-- Written by Tarun Sreenathan & Katarzyna Dobrzycka -->


<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="com.cs336.pkg.*"%>
<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*" %>
<jsp:useBean id="ctrl" scope="session" class="com.cs336.pkg.dbcalls"></jsp:useBean>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" name="viewport" content="text/html; charset=UTF-8, width=device-width, initial-scale=1">
<title>Log In</title>
<style>
html {
    display:table;
    margin:10px auto;
}
body {
    display:table-cell;
    vertical-align:middle;
}
h1,h2,h3,p,td{
	color:#EC407A;
	font-family:verdana;
}
</style>
</head>

<%
//CHECK LOGIN CREDENTIALS
		boolean manage_custSignedin = false;
		String signedin = (String)session.getAttribute("signedin");
		if(signedin == null ||
				signedin.equals("f")||
				signedin.equals("c")){
		}else{
			manage_custSignedin = true;
		}

//CHECK IF LOGIN CREDENTIALS SUBMITTED
		if(!manage_custSignedin){
			try {
				
				String un = request.getParameter("username");
				String pass = request.getParameter("password");
				
				if(request.getParameter("username").isEmpty()|| request.getParameter("password").isEmpty()){
					out.print("<tr><td><p>*All Fields Required</p></td></tr>");
				}
				else{
					boolean emailExists = ctrl.emailExists(un, true);
					boolean passCorrect = ctrl.passwordCheck(un, pass, true);
					if(!emailExists || !passCorrect){
						out.print("<tr><td><p>*Password or user name incorrect</p></td></tr>");
					}else{
						//out.print("<tr><td><p>*No account for this email</p></td></tr>");
						boolean isManager = ctrl.isManager(un, pass);
						if(isManager){
							signedin = "m";
						}else{
							signedin = "r";
						}
						manage_custSignedin = true;
						
						session.setAttribute("signedin", signedin); 
					}
				} 
				
				//db.closeConnection(con);
			}
			catch (Exception ex) {
			}
		}else{
//CHECK IF ATTEMPTING TO LOG OUT
			String log_out = request.getParameter("off");
			if(log_out != null && !log_out.isEmpty()){
				signedin = "f";
				manage_custSignedin = false;
				session.setAttribute("signedin", signedin);
			}
		}
	%>


<%
	if(!manage_custSignedin){
//IF NOT LOGGED IN PRINT LOG IN SCREEN
%>
<body>

<form>

<table>
<tr>
<td><h1>Manager and Customer Representative Log In</h1></td>
</tr>

<tr>
<td><input type="text" name="username" placeholder="Username"></td>
</tr>
<tr>
<td><input type="password" name="password" placeholder="Password"></td>
</tr>
<tr>
<td><input type="button" value="Cancel" onClick="window.location.href='index.jsp'"><input type="submit" value="Log In"></td>
</tr>
	
</table>

</form>

</body>

<% }else{ 
//DETERMINE WHETHER MANAGER OR CUSTOMER REP IS LOGGED IN
	String user = "";
	if(signedin.equals("m")){
		user = "Manager";
	}else{
		user = "Customer Representative";
	}

%>
	
	<table>
		<tr>
		<td><h1><%=user%> Home</h1></td>
		</tr>
		<form>
		<tr>
		<td><input type='submit' value='Log Out' name='off'></td>
		</tr>
		</form>
		
	
	</table>

		<ul>
		
		<li><a href="employeeinfo.jsp">Add/Edit/Delete employees</a></li>
		<li><a href="customerinfo.jsp">Add/Edit/Delete customers</a></li>
		<li><a href="reportgen.jsp">See financial reports and flight information</a></li>
		<%
			if(signedin.equals("r")){
				%>
					<li><a href="custrepreserve.jsp">Create receipt</a></li>
				<%
				
			}
				
		
		%>
		
		</ul>

<% } %>
</html>