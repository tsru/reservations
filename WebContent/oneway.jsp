<!-- Written by Tarun Sreenathan & Katarzyna Dobrzycka -->


<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="com.cs336.pkg.*"%>
<%@ page import="java.io.*,java.util.*,java.sql.*,java.util.Date,java.text.SimpleDateFormat"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*" %>
<jsp:useBean id="ctrl" scope="session" class="com.cs336.pkg.dbcalls"></jsp:useBean>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" name="viewport" content="text/html; charset=UTF-8, width=device-width, initial-scale=1">
<title>One Way Search</title>
<style>
html {
    display:table;
    margin:10px auto;
}
body {
    display:table-cell;
    vertical-align:middle;
}
h1,h2,h3,p,td{
	color:#EC407A;
	font-family:verdana;
}
</style>
</head>

<%
//CHECK LOG IN CREDENTIALS
		boolean custsignedin = false;
		String signedin = (String)session.getAttribute("signedin");
		String account = (String)session.getAttribute("account");
		String email = (String)session.getAttribute("email");
		if(signedin == null ||
				signedin.equals("m")||
				signedin.equals("r")||
				signedin.equals("f")){
		}else{
			custsignedin = true;
		}

//CHECK IF LOG OUT WAS PRESSED
		String log_out = request.getParameter("off");
		if(log_out != null && !log_out.isEmpty()){
			signedin = "f";
			custsignedin = false;
			session.setAttribute("signedin", signedin);
			session.removeAttribute("email");
		}
		
//GET LIST OF CITIES IN WHICH WE HAVE AIRPORTS
		Vector airports = ctrl.getAirportCities();
		
		
		
//PARSE SEARCH REQUEST
		String from = request.getParameter("from");
		String dateDepart = "";
		String dateDepart1 = "";
		String dateDepart2 = "";
		Vector results = new Vector();
		if(from!= null && !from.equals("")){
			//Search was sent
			dateDepart = request.getParameter("dateDepart");
			System.out.println(dateDepart);
			if(dateDepart!=null && !dateDepart.equals("")){
				//standard, not flexible	
				String to = request.getParameter("to");
				String timemin = request.getParameter("timeDepart1");
				String timemax = request.getParameter("timeDepart2");
				//String passengers = request.getParameter("passengers");
				String passengers = "1";
				results = ctrl.getFlightsOneWayByCity(from, to, dateDepart, timemin, timemax, passengers);
				
			}else{
				//flexible dates given
				dateDepart1 = request.getParameter("dateDepart1");
				dateDepart2 = request.getParameter("dateDepart2");
				String to = request.getParameter("to");
				String timemin = request.getParameter("timeDepart1");
				String timemax = request.getParameter("timeDepart2");
				//String passengers = request.getParameter("passengers");
				String passengers = "1";
				results = ctrl.getFlightsOneWayFlexByCity(from, to, dateDepart1, dateDepart2, timemin, timemax, passengers);
			}
			
		}
		
		
		
		
	%>

<body>
<%
if(custsignedin){
	%><form>
	<tr>
	<td><input type='submit' value='Log Out' name='off'></td>
	</tr>
	</form>
	<input type="button" value="My Account" onClick="window.location.href='custlogin.jsp'">
	<%
	
}else{
	%>
	<input type="button" value="Log In" onClick="window.location.href='custlogin.jsp'">
	
	
	<%	
	
	
}
	
		
%>
<input type='button' value='Home' onClick="window.location.href='index.jsp'">
	
	<h1>Search for flights:</h1>

<form>
<p>One Way Flights</p>
<ul>
<li>
<div>
<label>Depart from</label>
	<select name="from">
		<%
			for(int i = 0; i < airports.size(); i++){
				String xml = (String)airports.get(i);
				String port = ctrl.getTagContent(xml, "portCity");
				//String id = ctrl.getTagContent(xml, "airportID");
				out.println("<option value='"+port+"'>"+port+"</option>");
			}
		
		
		%>
	</select>
</div>
</li>
<li>
<div>
<label>Arrive at</label>
	<select name="to">
		<%
			for(int i = 0; i < airports.size(); i++){
				String xml = (String)airports.get(i);
				String port = ctrl.getTagContent(xml, "portCity");
				//String id = ctrl.getTagContent(xml, "airportID");
				out.println("<option value='"+port+"'>"+port+"</option>");
			}
		
		
		%>
	</select>
</div>
</li>
<li><input type="date" name="dateDepart" placeholder="Depart on (yyyy-MM-dd)"></li>
<li><input type="text" name="timeDepart1" placeholder="Depart between (HH:MM)"></li>
<li><input type="text" name="timeDepart2" placeholder="and (HH:MM)"></li>
<!-- <li><input type="number" name="passengers" placeholder="Passengers"></li>-->
</ul>
<input type="submit" value="Search">
</form>

<form>
<p>Flexible Date Search</p>
<ul>
<li>
<div>
<label>Depart from</label>
	<select name="from">
		<%
			for(int i = 0; i < airports.size(); i++){
				String xml = (String)airports.get(i);
				//String port = ctrl.getTagContent(xml, "portName");
				//String id = ctrl.getTagContent(xml, "airportID");
				//out.println("<option value='"+id+"'>"+port+"</option>");
				
				String port = ctrl.getTagContent(xml, "portCity");
				//String id = ctrl.getTagContent(xml, "airportID");
				out.println("<option value='"+port+"'>"+port+"</option>");
			}
		
		
		%>
	</select>
</div>
</li>
<li>
<div>
<label>Arrive at</label>
	<select name="to">
		<%
			for(int i = 0; i < airports.size(); i++){
				String xml = (String)airports.get(i);
				//String port = ctrl.getTagContent(xml, "portName");
				//String id = ctrl.getTagContent(xml, "airportID");
				//out.println("<option value='"+id+"'>"+port+"</option>");
				
				String port = ctrl.getTagContent(xml, "portCity");
				//String id = ctrl.getTagContent(xml, "airportID");
				out.println("<option value='"+port+"'>"+port+"</option>");
			}
		
		
		%>
	</select>
</div>
</li>
<li><input type="date" name="dateDepart1" placeholder="Depart between (yyyy-MM-dd)"></li>
<li><input type="date" name="dateDepart2" placeholder="and (yyyy-MM-dd)"></li>
*Max 7 days apart
<li><input type="text" name="timeDepart1" placeholder="Depart between (HH:MM)"></li>
<li><input type="text" name="timeDepart2" placeholder="and (HH:MM)"></li>
<!-- <li><input type="number" name="passengers" placeholder="Passengers"></li>-->
</ul>
<input type="submit" value="Search">
</form>

<%
	if(dateDepart!=null && !dateDepart.equals("")){
//PRINT SEARCH RESULTS BELOW THE SEARCH TOOLS
%>
<h2>Search Results</h2>

<ul>
	<%
		for(int i = 0; i < results.size(); i++){
			String xml = (String)results.get(i);
			String fn = ctrl.getTagContent(xml, "flightNumber");
			String aid = ctrl.getTagContent(xml, "airlineID");
			String nos= ctrl.getTagContent(xml, "numberOfSeats");
			String fr = ctrl.getTagContent(xml, "fareRestrict");
			String los = ctrl.getTagContent(xml, "lengthOfStay");
			String apr = ctrl.getTagContent(xml, "advancedPurchase");
			String f = ctrl.getTagContent(xml, "fare");
			String dp = ctrl.getTagContent(xml, "departPort");
			String dt = ctrl.getTagContent(xml, "departTime");
			String ap = ctrl.getTagContent(xml, "arrivePort");
			String at = ctrl.getTagContent(xml, "arriveTime");
			String dow = ctrl.getTagContent(xml, "dayOfWeek");
			String apd = ctrl.getTagContent(xml, "apdiscount");
			
			String departDateTime = dateDepart + " "+dt;
			int seatsLeft = Integer.parseInt(ctrl.seatsLeft(fn, aid, departDateTime));
			if(seatsLeft < 1){
				continue;
			}
			out.println("<li>Flight Number: "+fn+" Airline: " + aid);
			out.println("<br>Length Of Stay Restriction (min,max): " +los);
			out.println("<br>Fare Restrictions: "+fr+"<br>Advanced Purchase: "+apr+"<br>Advanced Purchase Discount: "+apd+"%");
			out.println("<br>Standard Fare: "+f+"<br>Departure Airport: "+dp+" on "+departDateTime);
			out.println("<br>Arrival Airport: "+ap+" at "+at);
			out.println("<br><input type=\"button\" value=\"Reserve\" onClick=\"window.location.href='reserve.jsp?type=1&flightNum="+fn+"&airlineID="+aid+"&date="+dateDepart+"&dateTime="+departDateTime+"'\"></li>");
			
		}
	
	
	
	%>


</ul>



<% }else if(from!= null && !from.equals("")){ %>
<h2>Search Results</h2>
	<%
	
		for(int i = 0; i < results.size(); i++){
			String xml = (String)results.get(i);
			String fn = ctrl.getTagContent(xml, "flightNumber");
			String aid = ctrl.getTagContent(xml, "airlineID");
			String nos= ctrl.getTagContent(xml, "numberOfSeats");
			String fr = ctrl.getTagContent(xml, "fareRestrict");
			String los = ctrl.getTagContent(xml, "lengthOfStay");
			String apr = ctrl.getTagContent(xml, "advancedPurchase");
			String f = ctrl.getTagContent(xml, "fare");
			String dp = ctrl.getTagContent(xml, "departPort");
			String dt = ctrl.getTagContent(xml, "departTime");
			String ap = ctrl.getTagContent(xml, "arrivePort");
			String at = ctrl.getTagContent(xml, "arriveTime");
			String dow = ctrl.getTagContent(xml, "dayOfWeek");
			String apd = ctrl.getTagContent(xml, "apdiscount");
			int dowint = Integer.parseInt(dow);
			
			int dayOfWeek = 0;
			String depart = dateDepart1;
			
			
			Date date=new SimpleDateFormat("yyyy-MM-dd").parse(dateDepart1); 
		    System.out.println(dateDepart1);
		    Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
			while(dayOfWeek!=dowint){
				System.out.println("INCREMENTING: "+dayOfWeek);
			    calendar.add(Calendar.DATE, 1);
			    depart = new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());
			    dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);			    
			}
			
			String departDateTime = depart + " "+dt;
			int seatsLeft = Integer.parseInt(ctrl.seatsLeft(fn, aid, departDateTime));
			if(seatsLeft < 1){
				continue;
			}
			out.println("<li>Flight Number: "+fn+" Airline: " + aid);
			out.println("<br>Length Of Stay Restriction (min,max): " +los);
			out.println("<br>Fare Restrictions: "+fr+"<br>Advanced Purchase: "+apr+"<br>Advanced Purchase Discount: "+apd+"%");
			out.println("<br>Standard Fare: "+f+"<br>Departure Airport: "+dp+" on "+departDateTime);
			out.println("<br>Arrival Airport: "+ap+" at "+at);
			out.println("<br><input type=\"button\" value=\"Reserve\" onClick=\"window.location.href='reserve.jsp?type=1&flightNum="+fn+"&airlineID="+aid+"&date="+depart+"&dateTime="+departDateTime+"'\"></li>");
			
		}




	%>

<%} %>

</body>
</html>