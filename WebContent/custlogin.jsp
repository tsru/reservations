<!-- Written by Tarun Sreenathan & Katarzyna Dobrzycka -->



<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="com.cs336.pkg.*"%>
<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*" %>
<jsp:useBean id="ctrl" scope="session" class="com.cs336.pkg.dbcalls"></jsp:useBean>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" name="viewport" content="text/html; charset=UTF-8, width=device-width, initial-scale=1">
<title>Log In</title>
<style>
html {
    display:table;
    margin:10px auto;
}
body {
    display:table-cell;
    vertical-align:middle;
}
h1,h2,h3,p,td{
	color:#EC407A;
	font-family:verdana;
}
</style>
</head>

<%

//CHECK SIGN IN CREDENTIALS
		boolean custsignedin = false;
		String signedin = (String)session.getAttribute("signedin");
		if(signedin == null ||
				signedin.equals("m")||
				signedin.equals("r")||
				signedin.equals("f")){
		}else{
			custsignedin = true;
		}
		
		if(!custsignedin){
//CHECK IF SIGNIN INFO WAS SUBMITTED
			try {
				
				String email = request.getParameter("email");
				String pass = request.getParameter("password");
				System.out.println("EMAIL: "+email+" PW: "+pass);
				if(request.getParameter("email").isEmpty()|| request.getParameter("password").isEmpty()){
					out.print("<tr><td><p>*All Fields Required</p></td></tr>");
				}
				else{
					boolean emailExists = ctrl.emailExists(email, false);
					boolean passCorrect = ctrl.passwordCheck(email, pass, false);
					if(!emailExists || !passCorrect){
						out.print("<tr><td><p>*Password or email incorrect</p></td></tr>");
					}else{
						custsignedin = true;
						signedin="c";
						session.setAttribute("signedin", signedin);
						session.setAttribute("email",email);
					}
				} 
			}
			catch (Exception ex) {
			}
		}else{
//CHECK IF LOG OUT WAS SELECTED
			String log_out = request.getParameter("off");
			if(log_out != null && !log_out.isEmpty()){
				signedin = "f";
				custsignedin = false;
				session.setAttribute("signedin", signedin);
				session.removeAttribute("email");
			}
		}
	%>


<%
	if(!custsignedin){
//PRINT LOG IN FORM
%>
<body>

<form>

<table>
<tr>
<td><h1>Customer Log In</h1></td>
</tr>

<tr>
<td><input type="text" name="email" placeholder="Email"></td>
</tr>
<tr>
<td><input type="password" name="password" placeholder="Password"></td>
</tr>
<tr>
<td>
	<input type="submit" value="Log In">
	<input type="button" value="Sign up" onClick="window.location.href='signup.jsp'">
	<input type="button" value="Cancel" onClick="window.location.href='index.jsp'">
	
</td>
</tr>
	
</table>

</form>

</body>

<% }else{
	
//PRINT ACCOUNT INFORMATION
		String email = (String)session.getAttribute("email");
		String infoxml = ctrl.custInfoEmail(email);
		System.out.println(infoxml);
		String accountNum = ctrl.getTagContent(infoxml, "accountNum");
			session.setAttribute("account",accountNum);
		String first = ctrl.getTagContent(infoxml, "first");
		String last = ctrl.getTagContent(infoxml, "last");
		String phone = ctrl.getTagContent(infoxml, "phone");
		String address = ctrl.getTagContent(infoxml, "address");
		String city = ctrl.getTagContent(infoxml, "city");
		String state = ctrl.getTagContent(infoxml, "state");
		String zip = ctrl.getTagContent(infoxml, "zip");
		String preferences= ctrl.getTagContent(infoxml, "preferences");
		String createDate = ctrl.getTagContent(infoxml, "createDate");
		String credit = ctrl.getTagContent(infoxml, "credit");
		
		
		
%>
	
	<table>
		<tr>
		<td><h1>My Account (Number: <%=accountNum%>)</h1></td>
		</tr>
		<form>
		<tr>
		<td><input type='submit' value='Log Out' name='off'><input type='button' value='Home' onClick="window.location.href='index.jsp'"></td>
		</tr>
		</form>
		
	
	</table>

		<ul>
			<li>Name: <%=first%> <%=last%></li>
			<li>Contact Information:</li>
				<ul>
					<li>Phone: <%=phone%></li>
					<li>Email: <%=email%></li>
				</ul>
			<li>Address: <%=address%><br><%=city%>, <%=state%>, <%=zip%></li>
			<li>Preferences: <%=preferences%></li>
			<li>Account created: <%=createDate%></li>
			<li>Credit info: <%=credit%></li>
			<li><a href="myreservations.jsp?accountNum=<%=accountNum%>">My reservations</a></li>
		</ul>

<% } %>
</html>