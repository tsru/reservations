<!-- Written by Katarzyna Dobrzycka -->


<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="com.cs336.pkg.*"%>
<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*" %>
<jsp:useBean id="ctrl" scope="session" class="com.cs336.pkg.dbcalls"></jsp:useBean>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" name="viewport" content="text/html; charset=UTF-8, width=device-width, initial-scale=1">
<title>Add Receipt</title>
<style>
html {
    display:table;
    margin:10px auto;
}
body {
    display:table-cell;
    vertical-align:middle;
}
h1,h2,h3,p,td{
	color:#EC407A;
	font-family:verdana;
}
</style>
</head>

<%

//CHECK SIGN IN CREDENTIALS, ONLY A CUSTOMER REPRESENTATIVE CAN ACCESS THIS PAGE
		boolean custSignedin = false;
		String signedin = (String)session.getAttribute("signedin");
		if(signedin == null ||
				signedin.equals("f")||
				signedin.equals("c")||
				signedin.equals("m")){
		}else{
			custSignedin = true;
		}
		

		if(custSignedin){
			//Vector employees = ctrl.getInfo(true);
			
			String error = "";
			if(request.getParameter("booking_fee")!=null && !request.getParameter("booking_fee").equals("")){
//CHECK IF NEW RESERVATION WAS SUBMITTED
				String reservationNum = request.getParameter("reservationNum");
				String customerID = request.getParameter("customerID");
				String totalFare = request.getParameter("totalFare");
				String dateCreated= request.getParameter("dateCreated");
				String booking_fee = request.getParameter("booking_fee");
				String customer_rep = request.getParameter("customer_rep");
				System.out.println(reservationNum + " "+customerID+" "+totalFare+" "+dateCreated+" "+booking_fee+" "+customer_rep);
				error = ctrl.addReserveCustRep(reservationNum, customerID, totalFare, dateCreated, booking_fee, customer_rep);
			}else if(request.getParameter("seatNum")!=null && !request.getParameter("seatNum").equals("")){
//CHECK IF NEW SECTION OF A RESERVATION WAS SUBMITTED
				String reservationNum = request.getParameter("reservationNum");
				String flightNum = request.getParameter("flightNum");
				String airlineID = request.getParameter("airlineID");
				String departDateTime = request.getParameter("departDateTime");
				String specialMeal= request.getParameter("specialMeal");
				String seatclass = request.getParameter("class");
				String seatNum= request.getParameter("seatNum");
				String fareApplied = request.getParameter("fareApplied");
				System.out.println(reservationNum + " "+flightNum+" "+airlineID+" "+departDateTime+" "+specialMeal+" "+seatclass+" "+seatNum+" "+fareApplied);
				error = ctrl.addReserveSectionCustRep(reservationNum, flightNum, airlineID, departDateTime, specialMeal, seatclass, seatNum, fareApplied);
			}
			
	%>



<body>
<input type="button" value="Cancel" onClick="window.location.href='emplogin.jsp'">

<p><%=error%></p>
<!-- ADD RESERVATION FORM -->
<form>
<h2>Add Reservation</h2>
<ul>
<li><input type="text" name="reservationNum" placeholder="Reservation Number"></li>
<li><input type="text" name="customerID" placeholder="Customer Account Number"></li>
<li><input type="text" name="totalFare" placeholder="Total Fare"></li>
<li><input type="text" name="dateCreated" placeholder="Date Created (yyyy-MM-dd)"></li>
<li><input type="text" name="booking_fee" placeholder="Booking Fee"></li>
<li><input type="text" name="customer_rep" placeholder="Customer Representative"></li>
<input type="submit" value="Add">
</ul>
</form>

<br><br>
<!-- ADD SECTION OF RESERVATION FORM -->
<form>
<h2>Add Section of a Reservation</h2>
<ul>
<li><input type="text" name="reservationNum" placeholder="Reservation Number"></li> 
<li><input type="text" name="flightNum" placeholder="Flight Number"></li>
<li><input type="text" name="airlineID" placeholder="Airline ID"></li>
<li><input type="text" name="departDateTime" placeholder="Depart Date and Time (yyyy-MM-dd HH:MM)"></li>
<li><input type="text" name="specialMeal" placeholder="Special Meal"></li>
<li><input type="text" name="class" placeholder="Class"></li>
<li><input type="text" name="seatNum" placeholder="Seat Number"></li>
<li><input type="text" name="fareApplied" placeholder="Fare"></li>
<input type="submit" value="Add">
</ul>
</form>



</body>

<% }else{ %>
	
	<p>You must be logged in as a customer representative to continue</p>
<input type="button" value="Log In" onClick="window.location.href='emplogin.jsp'">


<% } %>
</html>