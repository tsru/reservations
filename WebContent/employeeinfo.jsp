<!-- Written by Tarun Sreenathan & Katarzyna Dobrzycka -->


<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="com.cs336.pkg.*"%>
<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*" %>
<jsp:useBean id="ctrl" scope="session" class="com.cs336.pkg.dbcalls"></jsp:useBean>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" name="viewport" content="text/html; charset=UTF-8, width=device-width, initial-scale=1">
<title>Employee Info</title>

<style>
html {
    display:table;
    margin:10px auto;
}
body {
    display:table-cell;
    vertical-align:middle;
}
h1,h2,h3,p,td{
	color:#EC407A;
	font-family:verdana;
}
</style>
</head>

<%

//IF DELETE BUTTON WAS CLICKED TO REMOVE EMPLOYEE, RUN THE SQL TO REMOVE THAT EMPLOYEE
		if(request.getParameter("delete")!=null && !request.getParameter("delete").equals("")){
			ctrl.deleteInfo(request.getParameter("delete"),true);
		}

String error  = "";

//ADD EMPLOYEE IF NEW EMPLOYEE FORM WAS FILLED OUT
		if(request.getParameter("addssn")!=null && !request.getParameter("addssn").equals("")){
			String s = request.getParameter("addssn");
			String f = request.getParameter("first");
			String l = request.getParameter("last");
			String a = request.getParameter("address");
			String c = request.getParameter("city");
			String st = request.getParameter("state");
			String z = request.getParameter("zip");
			String p = request.getParameter("phone");
			String sd = request.getParameter("startdate");
			String h = request.getParameter("hourlyrate");
			error = ctrl.addEmployee(s, f, l, a, c, st, z, p, sd, h);
		}

//CHECK LOG IN
		boolean manage_custSignedin = false;
		String signedin = (String)session.getAttribute("signedin");
		if(signedin == null){
			signedin = "f";
		}
		if(signedin.equals("m") || signedin.equals("r")){
			manage_custSignedin = true;
		}
		
	if(manage_custSignedin){
//GET ALL EMPLOYEES' INFO
		Vector employees = ctrl.getInfo(true);

%>
<body>



<h1>Employee Info</h1>
<input type="button" value="Cancel" onClick="window.location.href='emplogin.jsp'">
<ul>
	<%
//PRINT EACH EMPLOYEE'S INFORMATION AS WELL AS A DELETE AND EDIT BUTTON THAT WILL
//PASS THE APPROPRIATE INFORMATION THROUGH THE URL
	for(int i = 0; i < employees.size(); i++){
		String xml = (String) employees.get(i);
		String ssn = ctrl.getTagContent(xml, "ssn");
		String first = ctrl.getTagContent(xml, "first");
		String last = ctrl.getTagContent(xml, "last");
		String phone = ctrl.getTagContent(xml, "phone");
		String address = ctrl.getTagContent(xml, "address");
		String city = ctrl.getTagContent(xml, "city");
		String state = ctrl.getTagContent(xml, "state");
		String zip = ctrl.getTagContent(xml, "zip");
		String start = ctrl.getTagContent(xml, "start");
		String hourlyRate = ctrl.getTagContent(xml, "hourlyRate");
		
		out.println("<li>"+first+" "+last+": ssn - "+ssn+"<br>"+
				"phone - "+phone+"<br>"+
				"address - "+address+" "+city+" "+state+" "+zip+"<br>"+
				"start date - "+start);
		
		if(signedin.equals("m")){
			out.println("<br>hourly rate - "+hourlyRate);
		}
		
		out.println("<input type=\"button\" value=\"delete\" onClick=\"window.location.href='employeeinfo.jsp?delete="+ssn+"'\">"+
				"<input type=\"button\" value=\"edit\" onClick=\"window.location.href='editInfo.jsp?ssn="+ssn+"'\">"+
				"</li>");
	}
	
	%>
</ul>
<!-- PRINT FORM FOR ADDING NEW EMPLOYEE -->
<h3>Add new employee: </h3>
<form>
<input type="text" name="addssn" placeholder="Social security number">
<input type="text" name="first" placeholder="First name">
<input type="text" name="last" placeholder="Last name">
<input type="text" name="address" placeholder="Address">
<input type="text" name="city" placeholder="City">
<input type="text" name="state" placeholder="State">
<input type="text" name="zip" placeholder="Zip">
<input type="text" name="phone" placeholder="Phone">
<input type="date" name="startdate" placeholder="Start date(YYYY-MM-DD)">
<input type="text" name="hourlyrate" placeholder="Hourly rate">
<input type="submit" value="Add employee">
<%=error%>
</form>





</body>

<% }else{ 
//IF NOT SIGNED IN PRINT BUTTON LEADING TO MANAGER/CUST REP LOG IN PAGE
		out.println("You are not signed in as a manager or customer representative");
		out.println("<a href=\"emplogin.jsp\">Go to login</a>");
			
	}

%>
</html>