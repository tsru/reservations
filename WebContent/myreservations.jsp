<!-- Written by Tarun Sreenathan & Katarzyna Dobrzycka -->


<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="com.cs336.pkg.*"%>
<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*" %>
<jsp:useBean id="ctrl" scope="session" class="com.cs336.pkg.dbcalls"></jsp:useBean>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" name="viewport" content="text/html; charset=UTF-8, width=device-width, initial-scale=1">
<title>My Reservations</title>
<style>
html {
    display:table;
    margin:10px auto;
}
body {
    display:table-cell;
    vertical-align:middle;
}
h1,h2,h3,p,td{
	color:#EC407A;
	font-family:verdana;
}
</style>
</head>

<%

//CHECK LOG IN CREDENTIALS
		boolean custsignedin = false;
		String signedin = (String)session.getAttribute("signedin");
		String account = (String)session.getAttribute("account");
		String email = (String)session.getAttribute("email");
		if(signedin == null ||
				signedin.equals("m")||
				signedin.equals("r")||
				signedin.equals("f")){
		}else{
			custsignedin = true;
		}
		
//IF DELETE RESERVATION BUTTON WAS PRESSED PROCEED TO DELETE THE RESERVATION	
		if(request.getParameter("delete")!=null && !request.getParameter("delete").equals("")){
			String toDelete = request.getParameter("delete");
			ctrl.deleteReservation(toDelete);
		}
	%>

<body>
<%
if(!custsignedin){
		out.println("You are not signed in as a customer");
		out.println("<a href=\"emplogin.jsp\">Go to login</a>");
}else{
//PRINT RESERVATIONS HELD BY USER
		String infoxml = ctrl.custInfoEmail(email);
		System.out.println(infoxml);
		
		String accountNum = ctrl.getTagContent(infoxml, "accountNum");
		String first = ctrl.getTagContent(infoxml, "first");
		String last = ctrl.getTagContent(infoxml, "last");
		String phone = ctrl.getTagContent(infoxml, "phone");
		String address = ctrl.getTagContent(infoxml, "address");
		String city = ctrl.getTagContent(infoxml, "city");
		String state = ctrl.getTagContent(infoxml, "state");
		String zip = ctrl.getTagContent(infoxml, "zip");
		String preferences= ctrl.getTagContent(infoxml, "preferences");
		String createDate = ctrl.getTagContent(infoxml, "createDate");
		String credit = ctrl.getTagContent(infoxml, "credit");
		
		
		
%>
	
	<h1>My Reservations</h1>
		<input type="button" value="Cancel" onClick="window.location.href='custlogin.jsp'">
	

		<ul>
			<%
				Vector myreserves = ctrl.getMyReservations(accountNum);
				for(int i = 0; i<myreserves.size(); i++){
					String xml = (String)myreserves.get(i);
					System.out.println(xml);
					String reservationNum = ctrl.getTagContent(xml, "reservationNum");
					String totalFare = ctrl.getTagContent(xml, "totalFare");
					String dateCreated = ctrl.getTagContent(xml, "dateCreated");
					
					out.println("<li>Reservation "+reservationNum+"<br>");
					out.println("Total fare: "+totalFare+ "<br>Date reserved: "+dateCreated);
					
					Vector legs = ctrl.getLegsOfReservation(reservationNum);
					out.println("<ul>");
					for(int j = 0; j <legs.size(); j++){
						String tmp = (String)legs.get(j);
						System.out.println(tmp);
						String flightNum = ctrl.getTagContent(tmp, "flightNumber");
						String airlineID = ctrl.getTagContent(tmp, "airlineID");
						String departDateTime = ctrl.getTagContent(tmp, "departDateTime");
						String specialMeal = ctrl.getTagContent(tmp, "specialMeal");
						String seatClass = ctrl.getTagContent(tmp, "class");
						String seatNum = ctrl.getTagContent(tmp, "seatNum");
						out.println("<li>");
						out.println("FlightNum: "+flightNum+"<br>Airline: "+airlineID+"<br>Depart Date and Time: "+departDateTime+"<br>Special Meal: "+specialMeal+"<br>Class: "+seatClass+"<br>Seat number: "+seatNum );
						out.println("</li>");
					}
					
					out.println("</ul>");
					out.println("<input type=\"button\" value=\"Cancel reservation\" onClick=\"window.location.href='myreservations.jsp?accountNum="+accountNum+"&delete="+reservationNum+"'\">");
					out.println("</li>");
				}
			
			
			
			
			%>
		</ul>

<% } %>
</body>
</html>