<!-- Written by Tarun Sreenathan & Katarzyna Dobrzycka -->


<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="com.cs336.pkg.*"%>
<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*" %>
<jsp:useBean id="ctrl" scope="session" class="com.cs336.pkg.dbcalls"></jsp:useBean>
<!DOCTYPE html>
<html>
<head>

<%
//CHECK IF EMPLOYEE OR CUSTOMER IDENTIFICATION WAS PASSED
//THIS WILL DETERMINE WHETHER TO SHOW EDIT EMPLOYEE OR EDIT CUSTOMER SCREEN
//THIS ALSO RETRIEVES THE IDENTIFIER FROM THE URL 
	String s1 = request.getParameter("ssn");
	String accountNum = request.getParameter("accountnum");
	String emp_or_cust = "";
	if(s1!=null && !s1.equals("")){
		emp_or_cust = "Employee";
	}else if(accountNum!=null && !accountNum.equals("")){
		emp_or_cust = "Customer";
	}
	System.out.println(emp_or_cust);


%>

<meta http-equiv="Content-Type" name="viewport" content="text/html; charset=UTF-8, width=device-width, initial-scale=1">
<title><%=emp_or_cust%> Info</title>
<style>
html {
    display:table;
    margin:10px auto;
}
body {
    display:table-cell;
    vertical-align:middle;
}
h1,h2,h3,p,td{
	color:#EC407A;
	font-family:verdana;
}
</style>
</head>

<%
		String error = "";
		String ssn = request.getParameter("ssn");
//CHECK WHETHER UPDATE FORM WAS SUBMITTED
		if(request.getParameter("newssn")!=null && !request.getParameter("newssn").equals("")){
	//IF EMPLOYEE INFO WAS SUBMITTED, THE EMPLOYEE GETS UPDATED
			//System.out.println("TEST TEST AAAA");
			String newssn = request.getParameter("newssn");
			String oldssn = request.getParameter("ssn");
			String newfirst = request.getParameter("first");
			String newlast = request.getParameter("last");
			String newphone = request.getParameter("phone");
			String newaddress = request.getParameter("address");
			String newcity = request.getParameter("city");
			String newstate = request.getParameter("state");
			String newzip = request.getParameter("zip");
			String newstart = request.getParameter("startdate");
			String newhourlyRate = request.getParameter("hourlyrate");
			error = ctrl.editEmployee(oldssn, newssn, newfirst, newlast, newaddress, newcity, newstate, newzip, newphone, newstart, newhourlyRate);
			ssn = newssn;
		}else if(request.getParameter("newemail")!=null && !request.getParameter("newemail").equals("")){
	//IF CUSTOMER INFO WAS SUBMITTED, THE CUSTOMER GETS UPDATED
			//System.out.println("TEST TEST BBBB");
			String newemail = request.getParameter("newemail");
			String newfirst = request.getParameter("first");
			String newlast = request.getParameter("last");
			String newphone = request.getParameter("phone");
			String newaddress = request.getParameter("address");
			String newcity = request.getParameter("city");
			String newstate = request.getParameter("state");
			String newzip = request.getParameter("zip");
			String newpreferences = request.getParameter("newpreferences");
			String newpassword = request.getParameter("newpassword");
			String newcredit = request.getParameter("newcredit");
			error = ctrl.editCustomer(accountNum, newfirst, newlast, newaddress, newcity, newstate, newzip, newphone, newemail, newpreferences, newcredit, newpassword);
			
		}
		System.out.println("ERROR: "+error);

		
//CHECK LOG IN CREDENTIALS
		boolean manage_custSignedin = false;
		String signedin = (String)session.getAttribute("signedin");
		if(signedin == null){
			signedin = "f";
		}
		if(signedin.equals("m") || signedin.equals("r")){
			manage_custSignedin = true;
		}
		
		
	if(manage_custSignedin){
		
		//System.out.println("TESTING TESTING CCCC");
//CHECK WHETHER TO DISPLAY EMPLOYEE OR CUSTOMER INFO
		if(s1!=null && !s1.equals("")){
			emp_or_cust = "Employee";
		}else if(accountNum!=null && !accountNum.equals("")){
			emp_or_cust = "Customer";
		}
		
		
		
	
		//if(request.getParameter("newssn")!=null && !request.getParameter("newssn").equals("")){
		if(emp_or_cust.equals("Employee")){
//GET EMPLOYEE INFO
			String xml = ctrl.employeeInfoSsn(ssn);
			String first = ctrl.getTagContent(xml, "first");
			String last = ctrl.getTagContent(xml, "last");
			String phone = ctrl.getTagContent(xml, "phone");
			String address = ctrl.getTagContent(xml, "address");
			String city = ctrl.getTagContent(xml, "city");
			String state = ctrl.getTagContent(xml, "state");
			String zip = ctrl.getTagContent(xml, "zip");
			String start = ctrl.getTagContent(xml, "start");
			String hourlyRate = ctrl.getTagContent(xml, "hourlyRate");
			%>
			
			<body>

<!-- PRINT FORM FOR EDITING EMPLOYEE AND PRE-POPULATE WITH EXISTING VALUES -->
			<h1>Edit employee: </h1>
			<form>
			<input type="hidden" name="ssn" value="<%=ssn%>">
			<input type="text" name="newssn" placeholder="Social security number" value="<%=ssn%>">
			<input type="text" name="first" placeholder="First name" value="<%=first%>">
			<input type="text" name="last" placeholder="Last name" value="<%=last%>">
			<input type="text" name="address" placeholder="Address" value="<%=address%>">
			<input type="text" name="city" placeholder="City" value="<%=city%>">
			<input type="text" name="state" placeholder="State" value="<%=state%>">
			<input type="text" name="zip" placeholder="Zip" value="<%=zip%>">
			<input type="text" name="phone" placeholder="Phone" value="<%=phone%>">
			<input type="date" name="startdate" placeholder="Start date(YYYY-MM-DD)" value="<%=start%>">
			<input type="number" name="hourlyrate" placeholder="Hourly rate" value="<%=hourlyRate%>">
			<input type="submit" value="Update employee">
			<input type="button" value="Cancel" onClick="window.location.href='employeeinfo.jsp'">
			<%=error%>
			</form>
			
			
			
			</body>
			
			
			<% 
			
			
		//}else if(request.getParameter("newemail")!=null && !request.getParameter("newemail").equals("")){
		}else if(emp_or_cust.equals("Customer")){
//GET CUSTOMER INFO
			String xml = ctrl.custInfoAccNum(accountNum);
			String first = ctrl.getTagContent(xml, "first");
			String last = ctrl.getTagContent(xml, "last");
			String phone = ctrl.getTagContent(xml, "phone");
			String email = ctrl.getTagContent(xml, "email");
			String address = ctrl.getTagContent(xml, "address");
			String city = ctrl.getTagContent(xml, "city");
			String state = ctrl.getTagContent(xml, "state");
			String zip = ctrl.getTagContent(xml, "zip");
			String preferences = ctrl.getTagContent(xml, "preferences");
			String createDate = ctrl.getTagContent(xml, "createDate");
			String password = ctrl.getTagContent(xml, "password");
			String credit = ctrl.getTagContent(xml, "credit");
			
			%>
			
			
			<body>
<!-- PRINT FORM FOR EDITING CUSTOMER AND PRE-POPULATE WITH EXISTING VALUES -->
			<h1>Edit customer (account number - <%=accountNum%>): </h1>
			<form>
			<input type="hidden" name="accountNum" value="<%=accountNum%>">
			<input type="text" name="newemail" placeholder="Email" value="<%=email%>">
			<input type="text" name="first" placeholder="First name" value="<%=first%>">
			<input type="text" name="last" placeholder="Last name" value="<%=last%>">
			<input type="text" name="address" placeholder="Address" value="<%=address%>">
			<input type="text" name="city" placeholder="City" value="<%=city%>">
			<input type="text" name="state" placeholder="State" value="<%=state%>">
			<input type="text" name="zip" placeholder="Zip" value="<%=zip%>">
			<input type="text" name="phone" placeholder="Phone" value="<%=phone%>">
			<input type="date" name="createdate" placeholder="Create date(YYYY-MM-DD)" value="<%=createDate%>">
			<input type="number" name="preferences" placeholder="Preferences" value="<%=preferences%>">
			<input type="number" name="password" placeholder="Password" value="<%=password%>">
			<input type="number" name="credit" placeholder="Credit card" value="<%=credit%>">
			<input type="submit" value="Update customer">
			<input type="button" value="Cancel" onClick="window.location.href='customerinfo.jsp'">
			<%=error%>
			</form>

			</body>
			
<%
		}
		
	 }else{ 
		
		out.println("You are not signed in as a manager or customer representative");
		out.println("<a href=\"emplogin.jsp\">Go to login</a>");
			
	}

%>
</html>