<!-- Written by Tarun Sreenathan & Katarzyna Dobrzycka -->

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="com.cs336.pkg.*"%>
<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*" %>
<jsp:useBean id="ctrl" scope="session" class="com.cs336.pkg.dbcalls"></jsp:useBean>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" name="viewport" content="text/html; charset=UTF-8, width=device-width, initial-scale=1">
<title>Home</title>
<style>
html {
    display:table;
    margin:10px auto;
}
body {
    display:table-cell;
    vertical-align:middle;
}
h1,h2,h3,p,td{
	color:#EC407A;
	font-family:verdana;
}
</style>
</head>

<%
//CHECK LOG IN CREDENTIALS
		boolean custsignedin = false;
		String signedin = (String)session.getAttribute("signedin");
		String account = (String)session.getAttribute("account");
		String email = (String)session.getAttribute("email");
		if(signedin == null ||
				signedin.equals("m")||
				signedin.equals("r")||
				signedin.equals("f")){
		}else{
			custsignedin = true;
		}

//LOG OUT IF LOG OUT BUTTON WAS CLICKED
		String log_out = request.getParameter("off");
		if(log_out != null && !log_out.isEmpty()){
			signedin = "f";
			custsignedin = false;
			session.setAttribute("signedin", signedin);
			session.removeAttribute("email");
		}
		
	%>

<body>
<!-- DISPLAY BUTTON TO REACH MANAGER/CUSTOMER REPRESENTATIVE LOG IN AND HOMEPAGE -->
<input type="button" value="Manager and Customer Rep Log In" onClick="window.location.href='emplogin.jsp'">
<%

//IF SIGNED IN DISPLAY LOG OUT BUTTON AND MY ACCOUNT, ELSE DISPLAY LOG IN
if(custsignedin){
	%><form>
	<tr>
	<td><input type='submit' value='Log Out' name='off'></td>
	</tr>
	</form>
	<input type="button" value="My Account" onClick="window.location.href='custlogin.jsp'">
	<%
	
}else{
	%>
	<input type="button" value="Log In" onClick="window.location.href='custlogin.jsp'">
	
	
	<%	
	
}
	


//DISPLAY SEARCH LINKS
%>
	
	<h1>Search for flights:</h1>

<form>
<table>
<tr>
<td><p>One Way Flights</p></td>
</tr>
<tr>
<td><input type="button" value="One Way Search" onClick="window.location.href='oneway.jsp'"></td>
</tr>
</table>
</form>
		
<form>
<table>
<tr>
<td><p>Round Trip Flights</p></td>
</tr>
<tr>
<td><input type="button" value="Round Trip Search" onClick="window.location.href='roundtrip.jsp'"></td>
</tr>
</table>
</form>		
		
<table>
<tr>
<td><p>Multi City Search</p></td>
</tr>
<tr>
<td><input type="button" value="Multi City Search" onClick="window.location.href='multicity.jsp'"></td>
</tr>
</table>		
		
		
<!-- DISPLAY UP TO THE TOP 10 BEST SELLING FLIGHTS -->
<h1>Top 10 Best Sellers: </h1>
		<ul>
			<%
				Vector bestSellers = ctrl.getBestSellers();
				for(int i = 0; i<bestSellers.size() && i < 10; i++){
					String xml = (String)bestSellers.get(i);
					String flightNum = ctrl.getTagContent(xml, "flightNum");
					String airlineID = ctrl.getTagContent(xml, "airlineID");
					String numberOfSeats = ctrl.getTagContent(xml, "numberOfSeats");
					String fareRestrict = ctrl.getTagContent(xml, "fareRestrict");
					String lengthOfStay = ctrl.getTagContent(xml, "lengthOfStayRestrict");
					String advancedPurchase = ctrl.getTagContent(xml, "advancedPurchase");
					String fare = ctrl.getTagContent(xml, "fare");
					String departPort = ctrl.getTagContent(xml, "departPort");
					String departTime = ctrl.getTagContent(xml, "departTime");
					String arrivePort = ctrl.getTagContent(xml, "arrivePort");
					String arriveTime = ctrl.getTagContent(xml, "arriveTime");
					Vector flightDays = ctrl.getFlightDays(flightNum, airlineID);
					String daysToPrint =""; 
					for(int k = 0; k < flightDays.size(); k++){
						if(k==0){
							daysToPrint = (String)flightDays.get(0);
						}else{
							daysToPrint = daysToPrint + ", " + (String)flightDays.get(k);
						}
					}
					
					
					//String seatsLeft = ctrl.seatsLeft(flightNum, airlineID);
					
					out.println("<li>Flight "+flightNum+" Airline: "+airlineID+"<br>");
					out.println("Depart from: "+ departPort+" at: "+departTime+" on: "+daysToPrint);
					out.println("<br> Arrive at: "+arrivePort+" at: "+arriveTime);
					out.println("<br>Fare restrictions: "+fareRestrict+"<br>Length of Stay Restriction: "+lengthOfStay+
							"<br>Advanced Purchase: "+advancedPurchase+"<br>Fare: "+fare);
					
				}
			
			
			
			
			%>
		</ul>

</body>
</html>