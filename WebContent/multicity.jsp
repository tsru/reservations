<!-- Written by Tarun Sreenathan & Katarzyna Dobrzycka -->


<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="com.cs336.pkg.*"%>
<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*" %>
<jsp:useBean id="ctrl" scope="session" class="com.cs336.pkg.dbcalls"></jsp:useBean>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" name="viewport" content="text/html; charset=UTF-8, width=device-width, initial-scale=1">
<title>Multi City Search</title>
<style>
html {
    display:table;
    margin:10px auto;
}
body {
    display:table-cell;
    vertical-align:middle;
}
h1,h2,h3,p,td{
	color:#EC407A;
	font-family:verdana;
}
</style>
</head>

<%
//CHECK LOG IN CREDENTIALS
		boolean custsignedin = false;
		String signedin = (String)session.getAttribute("signedin");
		String account = (String)session.getAttribute("account");
		String email = (String)session.getAttribute("email");
		if(signedin == null ||
				signedin.equals("m")||
				signedin.equals("r")||
				signedin.equals("f")){
		}else{
			custsignedin = true;
		}

//CHECK IF LOG OUT WAS PRESSED
		String log_out = request.getParameter("off");
		if(log_out != null && !log_out.isEmpty()){
			signedin = "f";
			custsignedin = false;
			session.setAttribute("signedin", signedin);
			session.removeAttribute("email");
		}

//GET AIRPORTCITIES
		//Vector airports = ctrl.getAirports();
		Vector airports = ctrl.getAirportCities();
		
		
		
//PARSE SEARCH
		String dateDepart1 = request.getParameter("dateDepart1");
		String dateDepart2 = "";
		String dateDepart3 = "";
		String dateDepart4 = "";
		String from1 = "";
		String from2 = "";
		String from3 = "";
		String from4 = "";
		String to1 = "";
		String to2 = "";
		String to3 = "";
		String to4 = "";
		
		
		Vector results1 = new Vector();
		Vector results2 = new Vector();
		Vector results3 = new Vector();
		Vector results4 = new Vector();
		
		if(dateDepart1!= null && !dateDepart1.equals("")){
			//Search was sent
			//dateDepart1 = request.getParameter("dateDepart1");
			dateDepart2 = request.getParameter("dateDepart2");
			dateDepart3 = request.getParameter("dateDepart3");
			dateDepart4 = request.getParameter("dateDepart4");
			from1 = request.getParameter("from1");
			from2 = request.getParameter("from2");
			from3 = request.getParameter("from3");
			from4 = request.getParameter("from4");
			to1 = request.getParameter("to1");
			to2 = request.getParameter("to2");
			to3 = request.getParameter("to3");
			to4 = request.getParameter("to4");
			
			if(dateDepart1!=null && !dateDepart1.equals("")){
				results1 = ctrl.getFlightsOneWayByCity(from1, to1, dateDepart1, "00:00", "23:59", "1");
			}
			if(dateDepart2!=null && !dateDepart2.equals("")){
				results2 = ctrl.getFlightsOneWayByCity(from2, to2, dateDepart2, "00:00", "23:59", "1");
			}
			if(dateDepart3!=null && !dateDepart3.equals("")){
				results3 = ctrl.getFlightsOneWayByCity(from3, to3, dateDepart3, "00:00", "23:59", "1");
			}
			if(dateDepart4!=null && !dateDepart4.equals("")){
				results4 = ctrl.getFlightsOneWayByCity(from4, to4, dateDepart4, "00:00", "23:59", "1");
			}
			
		}
				
	%>

<body>
<%
if(custsignedin){
	%><form>
	<tr>
	<td><input type='submit' value='Log Out' name='off'></td>
	</tr>
	</form>
	<input type="button" value="My Account" onClick="window.location.href='custlogin.jsp'">
	<%
	
}else{
	%>
	<input type="button" value="Log In" onClick="window.location.href='custlogin.jsp'">
	
	
	<%	
	
	
}
	
		
%>
	<input type='button' value='Home' onClick="window.location.href='index.jsp'">
	<h1>Multi City Search:</h1>

<form>
<ul>
<h2>First flight</h2>
<li>
<div>
<label>Depart from</label>
	<select name="from1">
		<%
			for(int i = 0; i < airports.size(); i++){
				String xml = (String)airports.get(i);
				String port = ctrl.getTagContent(xml, "portCity");
				//String id = ctrl.getTagContent(xml, "airportID");
				out.println("<option value='"+port+"'>"+port+"</option>");
			}
		
		
		%>
	</select>
</div>
</li>
<li>
<div>
<label>Arrive at</label>
	<select name="to1">
		<%
			for(int i = 0; i < airports.size(); i++){
				String xml = (String)airports.get(i);
				String port = ctrl.getTagContent(xml, "portCity");
				//String id = ctrl.getTagContent(xml, "airportID");
				out.println("<option value='"+port+"'>"+port+"</option>");
			}
		
		%>
	</select>
</div>
</li>
<li><input type="date" name="dateDepart1" placeholder="Depart on (yyyy-MM-dd)"></li>

<h2>Second flight</h2>
<li>
<div>
<label>Depart from</label>
	<select name="from2">
		<%
			for(int i = 0; i < airports.size(); i++){
				String xml = (String)airports.get(i);
				String port = ctrl.getTagContent(xml, "portCity");
				//String id = ctrl.getTagContent(xml, "airportID");
				out.println("<option value='"+port+"'>"+port+"</option>");
			}
		
		
		%>
	</select>
</div>
</li>
<li>
<div>
<label>Arrive at</label>
	<select name="to2">
		<%
			for(int i = 0; i < airports.size(); i++){
				String xml = (String)airports.get(i);
				String port = ctrl.getTagContent(xml, "portCity");
				//String id = ctrl.getTagContent(xml, "airportID");
				out.println("<option value='"+port+"'>"+port+"</option>");
			}
		
		%>
	</select>
</div>
</li>
<li><input type="date" name="dateDepart2" placeholder="Depart on (yyyy-MM-dd)"></li>


<h3>Third flight</h3>
<li>
<div>
<label>Depart from</label>
	<select name="from3">
		<%
			for(int i = 0; i < airports.size(); i++){
				String xml = (String)airports.get(i);
				String port = ctrl.getTagContent(xml, "portCity");
				//String id = ctrl.getTagContent(xml, "airportID");
				out.println("<option value='"+port+"'>"+port+"</option>");
			}
		
		
		%>
	</select>
</div>
</li>
<li>
<div>
<label>Arrive at</label>
	<select name="to3">
		<%
			for(int i = 0; i < airports.size(); i++){
				String xml = (String)airports.get(i);
				String port = ctrl.getTagContent(xml, "portCity");
				//String id = ctrl.getTagContent(xml, "airportID");
				out.println("<option value='"+port+"'>"+port+"</option>");
			}
		
		%>
	</select>
</div>
</li>
<li><input type="date" name="dateDepart3" placeholder="Depart on (yyyy-MM-dd)"></li>


<h3>Fourth Flight</h3>
<li>
<div>
<label>Depart from</label>
	<select name="from4">
		<%
			for(int i = 0; i < airports.size(); i++){
				String xml = (String)airports.get(i);
				String port = ctrl.getTagContent(xml, "portCity");
				//String id = ctrl.getTagContent(xml, "airportID");
				out.println("<option value='"+port+"'>"+port+"</option>");
			}
		
		
		%>
	</select>
</div>
</li>
<li>
<div>
<label>Arrive at</label>
	<select name="to4">
		<%
			for(int i = 0; i < airports.size(); i++){
				String xml = (String)airports.get(i);
				String port = ctrl.getTagContent(xml, "portCity");
				//String id = ctrl.getTagContent(xml, "airportID");
				out.println("<option value='"+port+"'>"+port+"</option>");
			}
		
		%>
	</select>
</div>
</li>
<li><input type="date" name="dateDepart4" placeholder="Depart on (yyyy-MM-dd)"></li>

<input type="submit" value="Search">
</ul>
</form>
<% if(dateDepart1!=null && !dateDepart1.equals("")){ 
//PRINT SEARCH RESULTS
%>

<h2>Search Results</h2>
<form action="reserve.jsp">
<input type="hidden" name="type" value="3">
<div>
<h3>First Flight</h3>

	<% 
		for(int i = 0; i < results1.size(); i++){
			String xml = (String)results1.get(i);
			String fn = ctrl.getTagContent(xml, "flightNumber");
			String aid = ctrl.getTagContent(xml, "airlineID");
			String nos= ctrl.getTagContent(xml, "numberOfSeats");
			String fr = ctrl.getTagContent(xml, "fareRestrict");
			String los = ctrl.getTagContent(xml, "lengthOfStay");
			String apr = ctrl.getTagContent(xml, "advancedPurchase");
			String f = ctrl.getTagContent(xml, "fare");
			String dp = ctrl.getTagContent(xml, "departPort");
			String dt = ctrl.getTagContent(xml, "departTime");
			String ap = ctrl.getTagContent(xml, "arrivePort");
			String at = ctrl.getTagContent(xml, "arriveTime");
			String dow = ctrl.getTagContent(xml, "dayOfWeek");
			String ddt = dateDepart1 + " " + dt;
			int seatsLeft = Integer.parseInt(ctrl.seatsLeft(fn, aid, ddt));
			if(seatsLeft < 1){
				continue;
			}
			out.println("<input type=\"radio\" name=\"depart1\" value=\""+fn+","+aid+","+ddt+","+dateDepart1+"\">Flight Number: "+fn+" Airline: " + aid);
			out.println("<br>Fare Restrictions: "+fr+"<br>Advanced Purchase: "+apr);
			out.println("<br>Length Of Stay Restriction (min,max): " +los);
			out.println("<br>Fare: "+f+"<br>Departure Airport: "+dp+" on "+ddt);
			out.println("<br>Arrival Airport: "+ap+" at "+at+"</input>");
			
		}
	%>
</div>


<div>
<h3>Second Flight</h3>

	<% 
		for(int i = 0; i < results2.size(); i++){
			String xml = (String)results2.get(i);
			String fn = ctrl.getTagContent(xml, "flightNumber");
			String aid = ctrl.getTagContent(xml, "airlineID");
			String nos= ctrl.getTagContent(xml, "numberOfSeats");
			String fr = ctrl.getTagContent(xml, "fareRestrict");
			String los = ctrl.getTagContent(xml, "lengthOfStay");
			String apr = ctrl.getTagContent(xml, "advancedPurchase");
			String f = ctrl.getTagContent(xml, "fare");
			String dp = ctrl.getTagContent(xml, "departPort");
			String dt = ctrl.getTagContent(xml, "departTime");
			String ap = ctrl.getTagContent(xml, "arrivePort");
			String at = ctrl.getTagContent(xml, "arriveTime");
			String dow = ctrl.getTagContent(xml, "dayOfWeek");
			String ddt = dateDepart2 + " " + dt;
			int seatsLeft = Integer.parseInt(ctrl.seatsLeft(fn, aid, ddt));
			if(seatsLeft < 1){
				continue;
			}
			out.println("<input type=\"radio\" name=\"depart2\" value=\""+fn+","+aid+","+ddt+","+dateDepart2+"\">Flight Number: "+fn+" Airline: " + aid);
			out.println("<br>Fare Restrictions: "+fr+"<br>Advanced Purchase: "+apr);
			out.println("<br>Length Of Stay Restriction (min,max): " +los);
			out.println("<br>Fare: "+f+"<br>Departure Airport: "+dp+" on "+ddt);
			out.println("<br>Arrival Airport: "+ap+" at "+at+"</input>");
			
		}
	%>
</div>


<div>
<h3>Third Flight</h3>

	<% 
		for(int i = 0; i < results3.size(); i++){
			String xml = (String)results3.get(i);
			String fn = ctrl.getTagContent(xml, "flightNumber");
			String aid = ctrl.getTagContent(xml, "airlineID");
			String nos= ctrl.getTagContent(xml, "numberOfSeats");
			String fr = ctrl.getTagContent(xml, "fareRestrict");
			String los = ctrl.getTagContent(xml, "lengthOfStay");
			String apr = ctrl.getTagContent(xml, "advancedPurchase");
			String f = ctrl.getTagContent(xml, "fare");
			String dp = ctrl.getTagContent(xml, "departPort");
			String dt = ctrl.getTagContent(xml, "departTime");
			String ap = ctrl.getTagContent(xml, "arrivePort");
			String at = ctrl.getTagContent(xml, "arriveTime");
			String dow = ctrl.getTagContent(xml, "dayOfWeek");
			String ddt = dateDepart3 + " " + dt;
			int seatsLeft = Integer.parseInt(ctrl.seatsLeft(fn, aid, ddt));
			if(seatsLeft < 1){
				continue;
			}
			out.println("<input type=\"radio\" name=\"depart3\" value=\""+fn+","+aid+","+ddt+","+dateDepart3+"\">Flight Number: "+fn+" Airline: " + aid);
			out.println("<br>Fare Restrictions: "+fr+"<br>Advanced Purchase: "+apr);
			out.println("<br>Length Of Stay Restriction (min,max): " +los);
			out.println("<br>Fare: "+f+"<br>Departure Airport: "+dp+" on "+ddt);
			out.println("<br>Arrival Airport: "+ap+" at "+at+"</input>");
			
		}
	%>
</div>


<div>
<h3>Fourth Flight</h3>

	<% 
		for(int i = 0; i < results4.size(); i++){
			String xml = (String)results4.get(i);
			String fn = ctrl.getTagContent(xml, "flightNumber");
			String aid = ctrl.getTagContent(xml, "airlineID");
			String nos= ctrl.getTagContent(xml, "numberOfSeats");
			String fr = ctrl.getTagContent(xml, "fareRestrict");
			String los = ctrl.getTagContent(xml, "lengthOfStay");
			String apr = ctrl.getTagContent(xml, "advancedPurchase");
			String f = ctrl.getTagContent(xml, "fare");
			String dp = ctrl.getTagContent(xml, "departPort");
			String dt = ctrl.getTagContent(xml, "departTime");
			String ap = ctrl.getTagContent(xml, "arrivePort");
			String at = ctrl.getTagContent(xml, "arriveTime");
			String dow = ctrl.getTagContent(xml, "dayOfWeek");
			String ddt = dateDepart4 + " " + dt;
			int seatsLeft = Integer.parseInt(ctrl.seatsLeft(fn, aid, ddt));
			if(seatsLeft < 1){
				continue;
			}
			out.println("<input type=\"radio\" name=\"depart4\" value=\""+fn+","+aid+","+ddt+","+dateDepart4+"\">Flight Number: "+fn+" Airline: " + aid);
			out.println("<br>Fare Restrictions: "+fr+"<br>Advanced Purchase: "+apr);
			out.println("<br>Length Of Stay Restriction (min,max): " +los);
			out.println("<br>Fare: "+f+"<br>Departure Airport: "+dp+" on "+ddt);
			out.println("<br>Arrival Airport: "+ap+" at "+at+"</input>");
			
		}
	%>
</div>

<input type="submit" value="Reserve">

</form>


<%} %>
</body>
</html>