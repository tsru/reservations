<!-- Written by Tarun Sreenathan -->


<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="com.cs336.pkg.*"%>
<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*" %>
<jsp:useBean id="ctrl" scope="session" class="com.cs336.pkg.dbcalls"></jsp:useBean>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" name="viewport" content="text/html; charset=UTF-8, width=device-width, initial-scale=1">
<title>Sign Up</title>
<style>
html {
    display:table;
    margin:10px auto;
}
body {
    display:table-cell;
    vertical-align:middle;
}
h1,h2,h3,p,td{
	color:#EC407A;
	font-family:verdana;
}
</style>
</head>

<body>


<!-- PRINT FORM FOR SIGNING UP -->
<form>
<table>
<tr>
<td><h1>Sign Up</h1></td>
</tr>
<tr>
<td><p>Basic Info</p></td>
</tr>
<tr>
<td><input type="text" name="fname" placeholder="First Name"></td>
<td><input type="text" name="lname" placeholder="Last Name"></td>
</tr>
<tr>
<td><input type="text" name="addr" placeholder="Address"></td>
<td><input type="text" name="state" placeholder="State"></td>
</tr>
<tr>
<td><input type="text" name="city" placeholder="City"></td>
<td><input type="text" name="zip" placeholder="Zip"></td>
</tr>
<tr>
<td><input type="text" name="ccard" placeholder="Credit Card"></td>
<td><input type="text" name="phone" placeholder="Phone"></td>
<tr>
<td><p>Login Info</p></td>
</tr>
<tr>
<td><input type="text" name="email" placeholder="Email"></td>
</tr>
<tr>
<td><input type="text" name="password" placeholder="Password"></td>
</tr>
<tr>
<td><input type="button" value="Cancel" onClick="window.location.href='index.jsp'"><input type="submit" value="Sign Up"></td>
</tr>
	<%
		try {
	//ATTEMPT TO SUBMIT SIGN UP DATA
			String fname = request.getParameter("fname");
			String lname = request.getParameter("lname");
			String address = request.getParameter("addr");
			String state = request.getParameter("state");
			String city = request.getParameter("city");
			String zip = request.getParameter("zip");
			String ccard = request.getParameter("ccard");
			String email = request.getParameter("email");
			String pass = request.getParameter("password");
			String phone = request.getParameter("phone");
			
			if(request.getParameter("fname").isEmpty()|| 
					request.getParameter("lname").isEmpty()|| 
					request.getParameter("addr").isEmpty()|| 
					request.getParameter("state").isEmpty()|| 
					request.getParameter("city").isEmpty()|| 
					request.getParameter("zip").isEmpty()|| 
					request.getParameter("ccard").isEmpty()|| 
					request.getParameter("email").isEmpty()|| 
					request.getParameter("password").isEmpty()||
					request.getParameter("phone").isEmpty()){
				out.print("<tr><td><p>*All Fields Required</p></td></tr>");
			}
			else{
		//CHECK IF INPUTS ARE VALID
				boolean emailExists = ctrl.emailExists(email, false);
				if(emailExists){
					out.print("<tr><td><p>*Email Already Exists</p></td></tr>");
				}else{
					
					ctrl.addCustomer(fname, lname, address, city, state, zip, phone, email, ccard, pass);
					out.print("<tr><td><p>Account Created</p></td></tr>");
				}
			} 
		}
		catch (Exception ex) {
		}
	%>
</table>
</form>
</body>

</html>